package info.ro.gadget.jdbi_codes.search;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.mutable.MutableLong;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.result.ResultIterator;
import org.jdbi.v3.core.statement.Query;
import org.jdbi.v3.core.statement.Update;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ivanceras.fluent.sql.SQL;
import com.ivanceras.fluent.sql.Breakdown;
import static com.ivanceras.fluent.sql.SQL.Statics.INSERT;
import static com.ivanceras.fluent.sql.SQL.Statics.SELECT;
import static com.ivanceras.fluent.sql.SQL.Statics.COUNT;


public abstract class AbstractSearcher< T extends Object > {
	private static Logger log = LoggerFactory.getLogger(new Object(){}.getClass().getEnclosingClass());
	//これが関与するのは基本的な検索ワードに関することまで。
	//その他具体的なところについては個別の事案として判定する
	
	Handle							handle;
	Class<T>						resultClazz	= null;
	
	//検索キー/カラムの組み合わせ。List内の集まりでorを取り、それらをまとめたもの同士でandを取る。
	private Map<String, List<SearchCondition>> wordConditions;
	
	private String sortColumn;	//ソート対象のカラム。
	private String order;		//昇順か降順か。
	private int searchPage = 1;		//検索のページ指定。ナシなら0となる。
	private int pageRows = 50;		//１ページにいくつ表示させるか。ナシなら50となる。
	
	
	public AbstractSearcher(Handle handle, Class<T> clazz) {
		this.handle = handle;
		this.resultClazz = clazz;
		this.wordConditions = new LinkedHashMap<String, List<SearchCondition>>();
	}
	
	public Set<String> getWordGroups() {
		return this.wordConditions.keySet();
	}
	
	public void setSortOrder(String column, String order) {
		column = this.requestToColumn(column);
		if(column != null) {
			this.sortColumn = column;
		}
		
		if("desc".equalsIgnoreCase(order)) {
			this.order = "desc";
		}
		else {
			this.order = "asc";
		}
	}
	
	public String getSortColumn() {
		return this.sortColumn;
	}
	public String getSortOrder() {
		return this.order;
	}
	
	public void addWordCondition(String searchGroup, String columnReq, String value) {
		this.addWordCondition(searchGroup, columnReq, value, true, true);
	}
	
	public void addWordCondition(String searchGroup, String columnReq, String value, boolean fuzzyLeft, boolean fuzzyRight) {
		//String column = this.requestToColumn(columnReq, this.childClazz != null);
		String column = this.requestToColumn(columnReq);
		log.debug("group: {} / column: {} / value: {}", searchGroup, column, value);
		if(searchGroup == null || "".equals(searchGroup) || column == null || "".equals(column)) {
			return;
		}
		List<SearchCondition> tmpConds = this.wordConditions.get(searchGroup);
		if(tmpConds == null) {
			tmpConds = new LinkedList<SearchCondition>();
			this.wordConditions.put(searchGroup, tmpConds);
		}
		
		SearchCondition newCond = new SearchCondition(column, value);
		newCond.setFuzzyCondition(fuzzyLeft, fuzzyRight);
		if(!newCond.isEqualToNothing()) {
			tmpConds.add(newCond);
		}
	}
	
	//特定のORグループの条件を削除したくなった時の措置
	public List<SearchCondition> clearSearchGroup(String searchGroup) {
		if(searchGroup == null || "".equals(searchGroup)) {
			return null;
		}
		return this.wordConditions.remove(searchGroup);
	}
	
	public void setPagination(String page, String rowsLimit) {
		int tmpPage = 1, tmpRows = 500;
		try {
			tmpPage = Integer.parseInt(page);
		}catch(NumberFormatException e) {}
		try {
			tmpRows = Integer.parseInt(rowsLimit);
		}catch(NumberFormatException e) {}
		this.setPagination(tmpPage, tmpRows);
	}
	
	public void setPagination(int page, int rowsLimit) {
		this.searchPage = page > 1 ? (page) : 1;
		this.pageRows = rowsLimit;
	}
	
	public int getSearchPage() {
		return this.searchPage;
	}
	public int getPageRows() {
		return this.pageRows;
	}
	
	private boolean addWhereCondition(SQL s) {
		SQL tmp = s.WHERE("1=1");
		{
			//設定したワード検索に関する事項をSQLに落とし込む。
			//グループごとにANDで分かれ、グループ内ではorでまとめられる。
			for(Entry<String, List<SearchCondition>> e : this.wordConditions.entrySet()) {
				List<SearchCondition> condList = e.getValue();
				if(condList != null && condList.size() > 0) {
					tmp = tmp.AND().openParen();{
						int i = 0;
						Map<String, List<SearchCondition>> tmpCondMap = new HashMap<String, List<SearchCondition>>();
						Set<String> orChainSet = new HashSet<String>();
						for(SearchCondition tmpCond:condList) {
							//1グループ内における同カラムの検索条件をまとめる
							List<SearchCondition> tmpList = tmpCondMap.get(tmpCond.getSearchColumn());
							if(tmpList == null) {
								tmpList = new LinkedList<SearchCondition>();
								tmpCondMap.put(tmpCond.getSearchColumn(), tmpList);
							}
							tmpList.add(tmpCond);
							//「in句でまとめられないもの/Likeやその他」が出現したら記録
							if(!tmpCond.isCapableForIn()) {
								orChainSet.add(tmpCond.getSearchColumn());
							}
						}
						
						//SQL組み立てフェーズ
						for(Entry<String, List<SearchCondition>> condEntry:tmpCondMap.entrySet()) {
							String tmpColumn = condEntry.getKey();
							//ORが各条件間でしか入らないのでこの措置
							if(i > 0) {
								tmp = tmp.OR(tmpColumn);
							}
							else {
								tmp = tmp.FIELD(tmpColumn);
							}
							if(orChainSet.contains(tmpColumn)) {
								//「in句でまとめられないカラム」に該当するならLIKE句でつなげる（現在は）
								for(SearchCondition tmpCond:condEntry.getValue()) {
									tmp = tmp.keyword("LIKE").VALUE(tmpCond.generateLikeKey());
								}
							}
							else {
								//「in句でまとめられるカラム」はin句にまとめる
								List<String> tmpValStrList = new LinkedList<String>();
								for(SearchCondition tmpCond:condEntry.getValue()) {
									tmpValStrList.add(tmpCond.getSearchKey());
								}
								tmp = tmp.IN(tmpValStrList.toArray());
							}
							i++;
						}
						
					}tmp = tmp.closeParen();
				}
			}
		}
		//最後に、個別の追加条件を加えて終了
		return this.addExtraSearchCondition(tmp);
	}
	
	private void addSortCondition(SQL s) {
		SQL tmp = s.ORDER_BY();
		if(this.sortColumn != null) {
			tmp = tmp.FIELD(this.sortColumn).keyword(order);
			tmp = tmp.comma();
		}
		this.addDefaultSortCondition(tmp);
	}
	
	private void addPaginateCondition(SQL s) {
		if(this.pageRows >= 0) {
			s.LIMIT(this.pageRows)
			.OFFSET(this.pageRows * (this.searchPage - 1));
		}
	}
	
	/*
		create temporary table if not exists parent_ids(parent_id int);
		delete from parent_ids;
		insert into parent_ids(parent_id) select distinct call_history.call_id from call_history left join called_result on call_history.call_id = called_result.call_id where called_result.customer_name like "%abd%" or call_history.ope_module_address like "";
		
		insert into parent_ids(parent_id) select distinct call_history.call_id from call_history left join called_result on call_history.call_id = called_result.call_id where called_result.customer_name like "%abd%" order by call_history.attribute limit 50 offset 50;
		
		select * from call_history where call_history.call_id in (select * from parent_ids);
		select * from called_result where called_result.call_id in (select * from parent_ids);
	 */
	
	public long count() {
		
		String searchTbl = this.getParentTableName();
		
		
		SQL sql = SELECT(COUNT("DISTINCT "+this.getPrimaryColumn())).FROM(searchTbl);
		if(!this.addWhereCondition(sql)) {
			return 0L;
		}
		Breakdown actual = sql.build();
		log.info(actual.getSql());
		Query countQuery = this.handle.createQuery(actual.getSql());
		int i = 0;
		for(Object val:actual.getParameters()) {
			countQuery = countQuery.bind(i,  val);
			i++;
		}
		return countQuery.mapTo(Long.class).first();
	}
	
	public long search(EachEntityListener<T> listener, boolean paginate) {
		String tmpTbl = "TMP_"+String.valueOf(System.currentTimeMillis());
		String tmpCol = "got_id";
		String searchTbl  = this.getParentTableName();
		
		
		//まず、これからのために一時テーブルを作成する
		this.handle.execute("CREATE TEMPORARY TABLE IF NOT EXISTS "+tmpTbl+"(got_id BIGINT)");
		this.handle.execute("DELETE FROM "+tmpTbl);
		
		//→検索結果表に乗せるレコードID一覧を使って子レコードも抽出するため
		SQL sql = INSERT().INTO(tmpTbl).openParen().FIELD(tmpCol).closeParen()
					.SELECT().DISTINCT(this.getPrimaryColumn())
					.FROM(searchTbl);
		if(!this.addWhereCondition(sql)) {
			this.handle.execute("DROP TABLE "+tmpTbl);
			return 0;
		}
		this.addSortCondition(sql);
		if(paginate) {
			this.addPaginateCondition(sql);
		}
		Breakdown actual = sql.build();
		
		//始めに、「一時テーブルに検索結果のID表を記述するSQL」を実行する
		//※この時点でソート、ページネーション済み
		Update firstQuery = handle.createUpdate(actual.getSql());
		int i = 0;
		log.info(actual.getSql());
		//System.out.println(actual.getSql());
		for(Object val:actual.getParameters()) {
			//System.out.println("["+i+"]:"+val.toString());
			firstQuery = firstQuery.bind(i, val);
			i++;
		}
		firstQuery.execute();
		
		
		//本体レコードの取得を行う
		SQL resultSql = SELECT(this.getParentTableName()+".*")
				.FROM(this.getParentTableName()).INNER_JOIN(tmpTbl)
					.ON(this.getPrimaryColumn(), tmpCol);
		this.addSortCondition(resultSql);
		Breakdown resultActual = resultSql.build();
		log.info(resultActual.getSql());
		Query resultQuery = handle.createQuery(resultActual.getSql());
		MutableLong retVal = new MutableLong(0);
		try(ResultIterator<T> it = resultQuery.mapToBean(this.resultClazz).iterator()){
			it.forEachRemaining((t)->{
				if(!listener.foreachEntity(t, retVal.getValue())) {
					it.close();
				}
				retVal.add(1);
			});
		}
		
		//最後に一時テーブルを削除
		this.handle.execute("DROP TABLE "+tmpTbl);
		return retVal.getValue();
	}
	
	/**
	 * 検索対象の主体となる親テーブル名を取得する（個別事案）
	 * 検索結果集計時に使用
	 * @return
	 */
	abstract protected String getParentTableName();
	
	/**
	 * 検索時の結合テーブル名を取得する（個別事案）
	 * 検索結果集計時に使用
	 * @return
	 */
	abstract protected String getSearchTableName();
	
	/**
	 * プライマリキーに該当するカラムを取得する（個別事案）
	 * @return
	 */
	abstract protected String getPrimaryColumn();
	
	/**
	 * デフォルトのソート順列を設定する
	 * @param s
	 */
	abstract protected void addDefaultSortCondition(SQL sql);
	
	/**
	 * 外部からのリクエストに基づいてカラム名を取得する
	 * @return
	 */
	abstract protected String requestToColumn(String request);
	
	/**
	 * 検索ワードとはまた違う検索条件を個別につけたい時のためのもの
	 * 例えば日時の指定。
	 * @return
	 * 条件として成立する物ならtrue。
	 * 検索してもヒットするものが出現しえないものならfalse
	 */
	abstract protected boolean addExtraSearchCondition(SQL sql);
	
}
