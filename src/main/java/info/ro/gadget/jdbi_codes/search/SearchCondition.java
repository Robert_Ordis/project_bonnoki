package info.ro.gadget.jdbi_codes.search;

/**
 * 検索に関するコンディションの指定
 * @author Robert_Ordis
 *
 */
public class SearchCondition {
	String	searchColumn = "";
	String	searchKey = "";
	boolean	fuzzyRight	= true;
	boolean	fuzzyLeft	= true;
	
	/**
	 * コンストラクタ。ここで指定したカラムとキーの変更は不可である
	 * @param searchColumn
	 * @param searchKey
	 */
	public SearchCondition(String searchColumn, String searchKey) {
		this.searchColumn = searchColumn == null ? "":searchColumn;
		this.searchKey = searchKey == null ? "":searchKey;
	}
	
	public String getSearchColumn() {
		return this.searchColumn;
	}
	
	public String getSearchKey() {
		return this.searchKey;
	}
	
	/**
	 * あいまい検索指定
	 * @param fuzzyRight	右側あいまい検索
	 * @param fuzzyLeft		左側あいまい検索
	 */
	public void setFuzzyCondition(boolean fuzzyLeft, boolean fuzzyRight) {
		this.fuzzyRight = fuzzyRight;
		this.fuzzyLeft = fuzzyLeft;
	}
	
	public boolean isCapableForIn() {
		return !(this.fuzzyLeft || this.fuzzyRight);
	}
	
	/**
	 * 実際に検索に用いるキーを取得する。
	 * @return
	 */
	public String generateLikeKey() {
		String	value = this.searchKey == null ? "":this.searchKey;
		value = value.replace("%", "\\%");
		value = value.replace("_", "\\_");
		if(this.fuzzyLeft) {
			value = "%"+value;
		}
		if(this.fuzzyRight) {
			value = value + "%";
		}
		return value;
	}
	
	/**
	 * 「何も入力していない」のと同等であるかどうか
	 * @return
	 */
	public boolean isEqualToNothing() {
		if(this.searchColumn == null || "".equals(this.searchColumn)) {
			return true;
		}
		if(this.searchKey == null || "".equals(this.searchKey)) {
			return true;
		}
		return false;
	}
	
}
