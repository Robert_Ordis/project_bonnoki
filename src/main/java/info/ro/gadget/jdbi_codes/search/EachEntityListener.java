package info.ro.gadget.jdbi_codes.search;

public interface EachEntityListener<T extends Object> {
	boolean	foreachEntity(T entity, long index);
}
