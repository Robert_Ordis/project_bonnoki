package info.ro.gadget.jdbi_codes.dao;

import java.util.Collection;
import java.util.List;

public interface EntityInfoDao <T extends Object>{
	/**
	 * 全レコードを取得する
	 * @return
	 */
	public List<T> getAll();
	
	/**
	 * 全登録数を取得する
	 * @return
	 */
	public long countAll();
	
	/**
	 * IDを一つ指定して取得する
	 * @param ownId
	 * @return
	 */
	public T getById(long ownId);
	
	/**
	 * IDを一つ指定し、FOR UPDATEとして取得する（トランザクション終了まで該当する行はロックされます）
	 * @param ownId
	 * @return
	 */
	public T getByIdForUpdate(long ownId);
	
	//-----以下、編集用
	
	/**
	 * レコードを新しく一つ追加する
	 * @param record
	 * @return 新規ID
	 */
	public long insert(T record);
	
	/**
	 * レコードを新しく一つ、一部カラムのみを対象として追加する
	 * @param record
	 * @param cols
	 * @return
	 */
	public long partialInsert(T record, Collection<String> cols);
	
	/**
	 * レコードをアップデートする
	 * @param record
	 * @return
	 */
	public int update(T record);
	
	/**
	 * レコードを新しく一つ、一部カラムのみを対象として追加する
	 * @param record
	 * @param cols
	 * @return
	 */
	public int partialUpdate(T record, Collection<String> cols);
	
	/**
	 * 指定したIDのレコードを削除する
	 * @param id
	 * @return
	 */
	public int delete(long id);
}
