package info.ro.gadget.jdbi_codes.field;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * DBのフィールド情報を一意に提供するためのクラス。
 * @author 
 *
 */
public abstract class DbFieldInfo {
	
	String fullInsert = null;
	String fullUpdate = null;
	
	/**
	 * テーブル名を取得する
	 * @return
	 */
	public abstract String getTableName();
	
	/**
	 * IDカラム名を取得する
	 * @param withTbl trueでtbl.idの形式となる
	 * @return
	 */
	public abstract String getIdColumn();
	public abstract String getIdColumn(boolean withTbl);
	
	/**
	 * リクエスト相当の情報からカラム名を取得する
	 * @param request カラム名。テーブル名.カラム名でも取得可。
	 * @param withTbl trueでtbl.columnの形式となる
	 * @return 当てはまるものがなければnullを返す
	 */
	public abstract String requestToColumn(String request);
	public abstract String requestToColumn(String request, boolean withTbl);
	
	
	/**
	 * 自動生成値を入れるカラムについての記述。
	 * @param withTbl trueでtbl.columnの形式で参照される。
	 * @return
	 */
	public abstract Map<String, String> getAutoColumnData(boolean withTbl);
	/**
	 * 自動生成値を入れるカラムについての記述。(withTbl = false)
	 * @return
	 */
	public abstract Map<String, String> getAutoColumnData();
	
	public abstract List<String> getColumns();
	
	private Set<String> makeColSet(Collection<String> reqs) {
		//リクエストされたカラム群から、実際にSQLに使える形に落とし込む
		if(reqs == null) {
			return null;
		}
		
		Set<String> tmpCols = new HashSet<String>();
		for(String c:reqs) {
			String tmp = this.requestToColumn(c);
			if(tmp != null) {
				tmpCols.add(tmp);
			}
		}
		return tmpCols;
	}
	
	/**
	 * INSERT文を生成する
	 * @param cols
	 * @return
	 */
	public final String generatePartialInsert(Collection<String> cols) {

		//insert into table(column, column) values(value, value)
		
		Set<String> tmpCols = makeColSet(cols);
		if(tmpCols == null || tmpCols.size() <= 0) {
			return null;
		}
		
		StringBuilder columnStatement = new StringBuilder();
		StringBuilder valuesStatement = new StringBuilder();
		int columnCount = 0;
		Map<String, String> autoCols = this.getAutoColumnData(false);
		
		columnStatement.append("INSERT INTO ");
		columnStatement.append(this.getTableName());
		columnStatement.append("(");
		
		valuesStatement.append(" VALUES(");
		
		for(String c:tmpCols) {
			String appendee = ":"+c;
			if(autoCols.containsKey(c)) {
				appendee = autoCols.get(c);
				if(appendee == null) {
					continue;
				}
			}
			if(columnCount > 0) {
				columnStatement.append(", ");
				valuesStatement.append(", ");
			}
			columnStatement.append(c);
			valuesStatement.append(appendee);
			columnCount = 1;
		}
		
		columnStatement.append(") ");
		valuesStatement.append(") ");
		
		columnStatement.append(valuesStatement);
		return columnStatement.toString();
	}
	
	/**
	 * フル仕様のINSERT文を生成する
	 * @return
	 */
	public final String generateInsert() {
		if(this.fullInsert == null) {
			this.fullInsert = this.generatePartialInsert(this.getColumns());
		}
		return this.fullInsert;
	}
	
	/**
	 * UPDATE文を生成する
	 * @param cols
	 * @return
	 */
	public final String generatePartialUpdate(Collection<String> cols) {
		//update table set col1 = :val1, col2 = :val2 where key = :key and key = :key

		Set<String> tmpCols = makeColSet(cols);
		if(tmpCols == null || tmpCols.size() <= 0) {
			return null;
		}
		
		Map<String, String> autoCols = this.getAutoColumnData(false);
		StringBuilder updateStatement = new StringBuilder();
		String keyCol = this.getIdColumn(false);
		int columnCount = 0;
		
		updateStatement.append("UPDATE ").append(this.getTableName()).append(" SET ");
		
		for(String c:tmpCols) {
			//キーカラム指定のものは除外。
			if(c.contentEquals(keyCol)) {
				continue;
			}
			String appendee = ":" + c;
			if(autoCols.containsKey(c)) {
				appendee = autoCols.get(c);
				//DB側自動生成ものの場合除外
				if(appendee == null) {
					continue;
				}
			}
			if(columnCount > 0) {
				updateStatement.append(", ");
			}
			updateStatement.append(c).append(" = ").append(appendee);
			columnCount = 1;
		}
		updateStatement.append(" WHERE ");
		updateStatement.append(keyCol).append(" = :").append(keyCol);
		
		return updateStatement.toString();
	}
	
	/**
	 * フル仕様のUPDATE文を生成する
	 * @return
	 */
	public final String generateUpdate() {
		if(this.fullUpdate == null) {
			this.fullUpdate = this.generatePartialUpdate(this.getColumns());
		}
		return this.fullUpdate;
	}
}
