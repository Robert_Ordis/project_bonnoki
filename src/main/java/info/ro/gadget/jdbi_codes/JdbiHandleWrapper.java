package info.ro.gadget.jdbi_codes;

import java.io.Closeable;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.SQLTransientException;

import javax.sql.DataSource;

import org.jdbi.v3.core.CloseException;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.HandleCallback;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.statement.UnableToCreateStatementException;
import org.jdbi.v3.core.statement.UnableToExecuteStatementException;
import org.sqlite.SQLiteErrorCode;
import org.sqlite.SQLiteException;

/**
 * JDBIのハンドルについて。
 * <pre>
 * ・トランザクション中に
 * 　デッドロックったとかが原因で処理を一度やり直すことになった場合に
 * 　一律で処理をやり直したいのでラッパー作成。
 * </pre>
 * @author Robert_Ordis
 *
 */
public class JdbiHandleWrapper implements Closeable{
	Handle h;
	public JdbiHandleWrapper(Handle h) {
		assert h != null;
		this.h = h;
	}
	public JdbiHandleWrapper(DataSource ds){
		assert ds != null:"data source must not be NULL";
		this.h = Jdbi.open(ds);
	}
	public JdbiHandleWrapper(Jdbi jdbi){
		assert jdbi != null;
		this.h = jdbi.open();
	}
	public Handle getHandle() {
		return this.h;
	}
	
	public <T> T getDao(Class<T> clazz) {
		return this.h.attach(clazz);
	}
	
	public <R,E extends Exception> R inTransaction(HandleCallback<R, E> callback) throws E{
		for(;;) {
			try {
				return this.h.inTransaction(callback);
			}catch(UnableToExecuteStatementException | UnableToCreateStatementException e) {
				Throwable cause = e.getCause();
				if(cause instanceof SQLiteException) {
					SQLiteException se = (SQLiteException)(cause);
					if(se.getResultCode() == SQLiteErrorCode.SQLITE_BUSY) {
						System.out.println("SQLITE_BUSY->may be transaction problem. retry.");
						continue;
					}
				}
				else if(cause instanceof SQLTransientException) {
					System.out.println("May be deadlock. or timeout or something else. retry it.");
					continue;
				}
				throw e;
			}catch(Throwable e) {
				throw e;
			}
		}
	}
	
	@Override
	public void close() throws CloseException {
		// TODO Auto-generated method stub
		this.h.close();
	}
}