package info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class VpvmNotFoundException extends VpvmBasisException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5572169273503827470L;
	
	protected VpvmNotFoundException(String msg) {
		super(msg);
	}
	protected VpvmNotFoundException(String msg, Throwable e) {
		super(msg, e);
	}
	
	public static VpvmNotFoundException makeInstance(String resourceName, String subject) {
		StringBuilder sb = new StringBuilder();
		sb.append(resourceName).append(":").append(subject).append(" -> NOT FOUND");
		return new VpvmNotFoundException(sb.toString());
	}
}
