package info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.db;

import java.util.Collection;
import java.util.List;

import org.jdbi.v3.core.result.ResultIterable;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.customizer.Define;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.stringtemplate4.UseStringTemplateEngine;

import info.ro.gadget.jdbi_codes.dao.EntityInfoDao;
import info.ro.gadget.jdbi_codes.field.DbFieldInfo;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.LetterEntity;

@RegisterBeanMapper(LetterEntity.class)
public interface LetterEntityDao extends EntityInfoDao<LetterEntity>{
	final DbFieldInfo tblInfo = LettersField.getInstance();
	String tbl = LettersField._TABLE;
	String pky = LettersField._PKEY;
	
	@Override
	@SqlQuery("SELECT * FROM " + tbl)
	List<LetterEntity> getAll();
	
	@Override
	@SqlQuery("SELECT COUNT(*) FROM " + tbl)
	long countAll();
	
	@Override
	@SqlQuery("SELECT * FROM "+ tbl + " WHERE " + pky + " = :id")
	LetterEntity getById(@Bind("id") long ownId);
	
	@Override
	@SqlQuery("SELECT * FROM "+ tbl + " WHERE " + pky + " = :id FOR UPDATE")
	LetterEntity getByIdForUpdate(@Bind("id") long ownId);
	
	@SqlQuery("SELECT * FROM "+ tbl + " WHERE " + LettersField.CHARA_ID + " = :id")
	ResultIterable<LetterEntity> getCharasVote(@Bind("id") long chara_id);
	//List<LetterEntity> getCharasVote(@Bind("id") long chara_id);
	
	@SqlQuery("SELECT * FROM "+ tbl + " WHERE " + LettersField.CHARA_ID + " = :id LIMIT :limit OFFSET :offset")
	ResultIterable<LetterEntity> getCharasVoteLimited(@Bind("id") long chara_id, @Bind("limit") long limit, @Bind("offset") long offset);
	
	
	//--------編集用クエリ--------
	@SqlUpdate("<query>")
	@GetGeneratedKeys(pky)
	@UseStringTemplateEngine
	long insert_(@Define("query") String query, @BindBean LetterEntity record);
	
	@Override
	default long partialInsert(LetterEntity record, Collection<String> cols) {
		String query = tblInfo.generatePartialInsert(cols);
		return query == null ? -1 : this.insert_(query, record);
	}
	
	@Override
	default long insert(LetterEntity record) {
		String query = tblInfo.generateInsert();
		return this.insert_(query, record);
	}
	
	@SqlUpdate("<query>")
	@UseStringTemplateEngine
	int update_(@Define("query") String query, @BindBean LetterEntity record);
	
	@Override
	default int partialUpdate(LetterEntity record, Collection<String> cols) {
		String query = tblInfo.generatePartialUpdate(cols);
		return query == null ? -1 : this.update_(query, record);
	}
	
	@Override
	default int update(LetterEntity record) {
		String query = tblInfo.generateUpdate();
		return this.update_(query, record);
	}
	
	@Override
	@SqlUpdate("DELETE FROM " + tbl + " WHERE " + pky + " = :id")
	int delete(@Bind("id") long id);
	
	//特定キャラに投票されたハガキを削除するためのクエリ
	@SqlUpdate("DELETE FROM " + tbl + " WHERE " + LettersField.CHARA_ID + " = :id")
	void deleteByCharaId(@Bind("id") long chara_id);
	
}
