/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.ro.gadget.votemaster.VampirePopularityVoteMaster.jetty.service;

import java.util.List;

import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.LetterEntity;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.db.LetterEntityDao;

@Component
public class HelloWorldService {

	@Value("${name:World}")
	private String name;
	
	@Value("${db.name}")
	private String path;
	
	@Autowired
	Jdbi jdbi;
	
	public String getHelloMessage() {
		//System.out.println("db path is "+db_loc);
		StringBuilder sb = new StringBuilder();
		sb.append("Hello ").append(this.name).append("\n");
		sb.append("Our DB path is ").append(this.path);
		
		
		
		return sb.toString();
	}

}
