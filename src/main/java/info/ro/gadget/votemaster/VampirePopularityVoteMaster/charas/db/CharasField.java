package info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.db;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import info.ro.gadget.jdbi_codes.field.DbFieldInfo;

public class CharasField extends DbFieldInfo{
	public static final String _TABLE = "charas";

	private static final String TSPECIFY = _TABLE + ".";
	private static final String WDOT = TSPECIFY;
//	private static final String WDOT = "";

	//カラム列の情報
	public static final String CHARA_ID		= WDOT + "chara_id";
	public static final String NAME			= WDOT + "name";
	public static final String CAME_FROM		= WDOT + "came_from";
	public static final String VOTED_SUM		= WDOT + "voted_sum";
	public static final String VOTED_MUL		= WDOT + "voted_mul";
	public static final String RES_ABS		= WDOT + "res_abs";
	public static final String RES_POSITIVE	= WDOT + "res_positive";
	public static final String BANNED		= WDOT + "banned";
	public static final String REMARKS		= WDOT + "remarks";
	
	public static final String _PKEY			= CHARA_ID;
	
	//基本として素の名前でやり取りする
	private Map<String, String> requestMap = new HashMap<String, String>();
	private Map<String, String> summaryMap = new HashMap<String, String>();
	private Map<String, String> autoColDat = new HashMap<String, String>();
	private List<String> columns = new LinkedList<String>();
	
	private static CharasField instance = null;
	
	private CharasField() {
		//列データをプログラムで取り扱えるようもろもろ定義
		this.putColData(CHARA_ID,		"キャラクタID"			);
		this.putColData(NAME,			"キャラクタ名"			);
		this.putColData(CAME_FROM,		"出典作品"				);
		this.putColData(VOTED_SUM,		"投票された数"			);
		this.putColData(VOTED_MUL,		"投票された係数"		);
		this.putColData(RES_ABS,		"計算結果（絶対値）"	);
		this.putColData(RES_POSITIVE,	"正の数かどうか"		);
		this.putColData(BANNED,		"亜空間行キャラかどうか");
		this.putColData(REMARKS,		"備考"					);
		
		//値が自動生成される列を定義。nullではINSERT/UPDATEに記述されない。
		//null以外では、固定的にクエリを記述する（CURRENT_TIMESTAMPとか）
		this.remarkAsAuto(_PKEY,		null);
	}
	
	public static CharasField getInstance() {
		if(instance == null) {
			instance = new CharasField();
		}
		return instance;
	}
	
	private void remarkAsAuto(String col, String value) {
		String[] exploded = col.split("\\.");
		String c = exploded[exploded.length - 1];
		autoColDat.put(c, value);
	}
	
	private void putColData(String col, String summary) {
		String[] exploded = col.split("\\.");
		String c;
		c = exploded[exploded.length - 1];
		columns.add(c);
		requestMap.put(c, c);
		summaryMap.put(c, summary);
	}
	
	@Override
	public String getTableName() {
		// TODO Auto-generated method stub
		return _TABLE;
	}

	@Override
	public String getIdColumn() {
		return this.getIdColumn(false);
	}

	@Override
	public String getIdColumn(boolean withTbl) {
		String[] exploded = _PKEY.split("\\.");
		String c = exploded[exploded.length - 1];
		return withTbl ? TSPECIFY + c : c;
	}

	@Override
	public String requestToColumn(String request) {
		return this.requestToColumn(request, false);
	}

	@Override
	public String requestToColumn(String request, boolean withTbl) {
		request = request.toLowerCase();
		String ret = this.requestMap.get(request);
		if(ret == null) {
			request = request.replaceAll(_TABLE + "\\.", "");
			ret = this.requestMap.get(request);
		}
		
		if(ret != null && withTbl) {
			ret = TSPECIFY + ret;
		}
		return ret;
	}
	
	@Override
	public Map<String, String> getAutoColumnData() {
		return this.getAutoColumnData(false);
	}

	@Override
	public Map<String, String> getAutoColumnData(boolean withTbl) {
		Map<String, String> retMap = new HashMap<String, String>();
		for(Entry<String, String>e: this.autoColDat.entrySet()) {
			String c = e.getKey();
			String v = e.getValue();
			if(withTbl) {
				c = TSPECIFY + c;
			}
			retMap.put(c, v);
		}
		return retMap;
	}
	
	@Override
	public List<String> getColumns(){
		return this.columns;
	}

}
