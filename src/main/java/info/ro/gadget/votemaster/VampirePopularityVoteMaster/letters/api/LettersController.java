package info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.api;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.CharaEntity;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.exception.VpvmBasisException;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.form.InputValid;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.form.PostValid;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.LetterEntity;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.api.form.LettersForm;

@RestController
@RequestMapping("/letters")
@CrossOrigin
public class LettersController {
	
	@Autowired
	LettersService service;
	
	/*
	@GetMapping({"/", ""})
	@ResponseBody
	public List<LetterEntity> getList(@RequestParam Map<String, String> params){
		System.out.println(params.get("keyword"));
		return service.getAll();
	}
	*/
	
	@GetMapping({"/", ""})
	@ResponseBody
	public List<LetterEntity> getList(
			@RequestParam(defaultValue = "") String search_col,
			@RequestParam(defaultValue = "") String search_key){
		//System.out.println(params.get("keyword"));
		return service.getSearched(search_col, search_key);
	}
	
	@GetMapping("/{id}")
	@ResponseBody
	public LetterEntity getById(@PathVariable("id") String id, HttpServletResponse response) throws VpvmBasisException {
		LetterEntity ret = service.getById(id);
		if(ret == null) {
			response.setStatus(404);
		}
		return ret;
	}
	
	@PostMapping({"/", ""})
	@ResponseBody
	public long postEntity(@Validated({PostValid.class, InputValid.class}) LettersForm form) throws VpvmBasisException {
		//form.putParams(null);
		return service.postFromForm(form);
		//return 0L;
	}
	@PutMapping("/{id}")
	@ResponseBody
	public int putById(@PathVariable("id") String id, @Validated({InputValid.class}) LettersForm form) throws VpvmBasisException {
		return service.putFromForm(id, form);
	}
	@DeleteMapping("/{id}")
	@ResponseBody
	public int deleteById(@PathVariable("id") String id) throws VpvmBasisException {
		return service.deleteByid(id);
	}
	
	@DeleteMapping({"/bulk-delete"})
	@ResponseBody
	public int bulkDelete(@RequestParam String[] letter_ids) throws VpvmBasisException {
		return service.bulkDelete(Arrays.asList(letter_ids));
	}
}
