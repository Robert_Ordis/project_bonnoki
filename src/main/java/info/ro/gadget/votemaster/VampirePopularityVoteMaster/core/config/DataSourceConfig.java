package info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.config;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ResourceLoader;
import org.sqlite.SQLiteDataSource;

@Configuration
@PropertySource("classpath:META-INF/application.properties")
public class DataSourceConfig {
	
	@Value("${db.name}")
	String name;
	
	@Autowired
	ResourceLoader resourceLoader;
	
	@Bean("my-sqlite")
	public DataSource sqliteDataSource() throws IOException, URISyntaxException {
		//実行時のディレクトリにDBを作る。なければリソースから取ってくる。
		File f = new File(this.name);
		String path = this.name;
		if(!f.isAbsolute()) {
			path = FilenameUtils.separatorsToUnix(System.getProperty("user.dir")) + "/" + this.name;
			f = new File(path);
		}

		System.out.println("treat db in "+path);
		if(!f.exists()) {
			//コピー。リソースのファイルto curr。
			//File src = ResourceUtils.getFile("classpath:META-INF/database.sqlite3");
			try(InputStream is = resourceLoader.getResource("classpath:META-INF/database.sqlite3").getInputStream()){
				FileUtils.copyInputStreamToFile(is, f);
			}
		}
		SQLiteDataSource ds = new SQLiteDataSource();
		ds.setUrl("jdbc:sqlite:" + path);
		
		return ds;
	}
	
}
