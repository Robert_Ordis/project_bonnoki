package info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.api.form;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.data.util.Pair;

import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.exception.VpvmInvalidParamsException;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.form.EntityParamsPutter;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.form.InputValid;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.form.PostValid;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.LetterConstants;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.LetterEntity;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.db.LettersField;

public class LettersForm implements EntityParamsPutter<LetterEntity>{
	private static Logger log = LoggerFactory.getLogger(new Object(){}.getClass().getEnclosingClass());
	
	private final BigInteger ZERO = new BigInteger("0");
	
	private String voter_name = null;
	private String voter_address = null;
	private String voted_name = null;
	
	@NotNull(groups = {PostValid.class}, message = "chara_id: mandatory")
	@Pattern(groups = {InputValid.class}, regexp = "^\\d+$", message = "chara_id: must be positive integer")
	private String chara_id = null;
	
	private String chara_variation = null;
	
	@NotBlank(groups = {PostValid.class}, message = "voted_num: mandatory")
	@Pattern(groups = {InputValid.class}, regexp = "-?[0-9]+", message = "voted_num: must be integer.")
	private String voted_num = null;
	
	@Pattern(groups = {InputValid.class}, regexp = "^\\d+$", message = "operation: must be integer.")
	private String operation = null;
	
	private String remarks = null;
	@Override
	public Pair<LetterEntity, Set<String>> makeEntity(LetterEntity src) throws VpvmInvalidParamsException {
		// TODO Auto-generated method stub
		Set<String> updateCols = new HashSet<String>();
		Map<String, String> errMap = new HashMap<String, String>();
		LetterEntity tmp = new LetterEntity();
		if(src == null) {
			src = new LetterEntity();
		}
		BeanUtils.copyProperties(src, tmp);
		
		log.info("voter_name: {}", voter_name);
		log.info("voter_address: {}", voter_address);
		log.info("voted_name: {}", voted_name);
		
		log.info("chara_id: {}", chara_id);
		log.info("chara_variation: {}", chara_variation);
		
		log.info("voted_num: {}", voted_num);
		log.info("operation: {}", operation);
		log.info("remarks: {}", remarks);
		
		if(voter_name != null) {
			tmp.setVoter_name(voter_name);
			updateCols.add(LettersField.VOTER_NAME);
		}
		if(voter_address != null) {
			tmp.setVoter_address(voter_address);
			updateCols.add(LettersField.VOTER_ADDRESS);
		}
		if(voted_name != null) {
			tmp.setVoted_name(voted_name);
			updateCols.add(LettersField.VOTED_NAME);
		}
		if(chara_id != null) {
			long id = Long.parseLong(chara_id);
			tmp.setChara_id(id);
			updateCols.add(LettersField.CHARA_ID);
		}
		if(chara_variation != null) {
			tmp.setChara_variation(chara_variation);
			updateCols.add(LettersField.CHARA_VARIATION);
		}
		if(operation != null) {
			int ope = Integer.parseInt(operation);
			switch(ope) {
			case LetterConstants.OPE_MUL:
			case LetterConstants.OPE_ADD:
				break;
			default:
				ope = LetterConstants.OPE_ADD;
			}
			tmp.setOperation(ope);
			updateCols.add(LettersField.OPERATION);
		}
		if(remarks != null) {
			tmp.setRemarks(remarks);
			updateCols.add(LettersField.REMARKS);
		}
		
		if(voted_num != null) {
			tmp.setVoted_num(voted_num);
			if(ZERO.equals(tmp.refVoted_num()) && tmp.getOperation() != LetterConstants.OPE_ADD) {
				errMap.put("voted_num", "Must not be 0 in multipling vote.");
			}
			updateCols.add(LettersField.VOTED_NUM);
		}
		
		
		tmp.setVoted_date(System.currentTimeMillis());
		updateCols.add(LettersField.VOTED_DATE);

		if(errMap.size() > 0) {
			throw VpvmInvalidParamsException.makeInstance(errMap);
		}
		
		return Pair.of(tmp, updateCols);
	}
	public String getVoter_name() {
		return voter_name;
	}
	public void setVoter_name(String voter_name) {
		this.voter_name = voter_name;
	}
	public String getVoter_address() {
		return voter_address;
	}
	public void setVoter_address(String voter_address) {
		this.voter_address = voter_address;
	}
	public String getVoted_name() {
		return voted_name;
	}
	public void setVoted_name(String voted_name) {
		this.voted_name = voted_name;
	}
	public String getChara_id() {
		return chara_id;
	}
	public void setChara_id(String chara_id) {
		this.chara_id = chara_id;
	}
	public String getChara_variation() {
		return chara_variation;
	}
	public void setChara_variation(String chara_variation) {
		this.chara_variation = chara_variation;
	}
	public String getVoted_num() {
		return voted_num;
	}
	public void setVoted_num(String voted_num) {
		this.voted_num = voted_num;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
}
