/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.ro.gadget.votemaster.VampirePopularityVoteMaster;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@Configuration
@EnableAutoConfiguration
@ComponentScan("info.ro.gadget")
public class VampireVoteMasterApplication {
	
	@Value("${boot.browser}")
	String boot;
	
	@Value("${server.port}")
	String port;
	
	@EventListener(ApplicationReadyEvent.class)
	public void afterStart() {
		if(Boolean.parseBoolean(boot)) {
			Desktop desktop = Desktop.getDesktop();
			try {
				desktop.browse(new URI("http://localhost:"+port+"/"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) throws Exception {
		/*
		ProtectionDomain domain = VampireVoteMasterApplication.class.getProtectionDomain();
	    URL location = domain.getCodeSource().getLocation();
	    WebAppContext webapp = new WebAppContext();
	    webapp.setContextPath("/");
	    webapp.setWar(location.toExternalForm());
	    server.setHandler(webapp);
	    */
		SpringApplicationBuilder builder = new SpringApplicationBuilder(VampireVoteMasterApplication.class);
		builder.headless(false);
		builder.run(args);
		//SpringApplication.run(VampireVoteMasterApplication.class, args);
	}
	
}
