package info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.api.form;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotBlank;

import org.springframework.beans.BeanUtils;
import org.springframework.data.util.Pair;

import info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.CharaEntity;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.db.CharasField;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.exception.VpvmInvalidParamsException;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.form.EntityParamsPutter;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.form.PostValid;

public class CharasForm implements EntityParamsPutter<CharaEntity>{
	
	@NotBlank(groups = {PostValid.class}, message = "name: mandatory")
	String name = null;
	
	String came_from = null;
	String banned = null;
	String remarks = null;
	
	String recalc = null;
	
	@Override
	public Pair<CharaEntity, Set<String>> makeEntity(CharaEntity src) throws VpvmInvalidParamsException{
		// TODO Auto-generated method stub
		Set<String> updateCols = new HashSet<String>();
		Map<String, String> errMap = new HashMap<String, String>();
		CharaEntity tmp = new CharaEntity();
		
		boolean calc_needed = false;
		
		if(src == null) {
			src = new CharaEntity();
			calc_needed = true;
		}
		BeanUtils.copyProperties(src, tmp);
		
		if(name != null) {
			tmp.setName(name);
			updateCols.add(CharasField.NAME);
		}
		
		if(came_from != null) {
			tmp.setCame_from(came_from);
			updateCols.add(CharasField.CAME_FROM);
		}
		
		if(banned != null) {
			tmp.setBanned(Boolean.parseBoolean(banned));
			updateCols.add(CharasField.BANNED);
		}
		
		if(remarks != null) {
			tmp.setRemarks(remarks);
			updateCols.add(CharasField.REMARKS);
		}
		
		if(recalc != null) {
			calc_needed = true;
		}
		
		if(calc_needed) {
			updateCols.add(CharasField.VOTED_MUL);
			updateCols.add(CharasField.VOTED_SUM);
			updateCols.add(CharasField.RES_ABS);
			updateCols.add(CharasField.RES_POSITIVE);
		}
		
		if(errMap.size() > 0) {
			throw VpvmInvalidParamsException.makeInstance(errMap);
		}
		
		return Pair.of(tmp, updateCols);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCame_from() {
		return came_from;
	}

	public void setCame_from(String came_from) {
		this.came_from = came_from;
	}

	public String getBanned() {
		return banned;
	}

	public void setBanned(String banned) {
		this.banned = banned;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRecalc() {
		return recalc;
	}

	public void setRecalc(String re_calculate) {
		this.recalc = re_calculate;
	}
	
}
