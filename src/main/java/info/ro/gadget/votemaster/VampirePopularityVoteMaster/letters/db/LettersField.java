package info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.db;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import info.ro.gadget.jdbi_codes.field.DbFieldInfo;

public class LettersField extends DbFieldInfo{
	public static final String _TABLE = "letters";

	private static final String TSPECIFY = _TABLE + ".";
	private static final String WDOT = TSPECIFY;
//	private static final String WDOT = "";

	//カラム列の情報
	public static final String LETTER_ID			= WDOT + "letter_id";
	public static final String VOTER_NAME		= WDOT + "voter_name";
	public static final String VOTER_ADDRESS	= WDOT + "voter_address";
	public static final String VOTED_NAME		= WDOT + "voted_name";
	public static final String CHARA_ID			= WDOT + "chara_id";
	public static final String CHARA_VARIATION	= WDOT + "chara_variation";
	public static final String VOTED_NUM			= WDOT + "voted_num";
	public static final String OPERATION			= WDOT + "operation";
	public static final String VOTED_DATE		= WDOT + "voted_date";
	public static final String REMARKS			= WDOT + "remarks";
	
	public static final String _PKEY				= LETTER_ID;
	
	//基本として素の名前でやり取りする
	private Map<String, String> requestMap = new HashMap<String, String>();
	private Map<String, String> summaryMap = new HashMap<String, String>();
	private Map<String, String> autoColDat = new HashMap<String, String>();
	private List<String> columns = new LinkedList<String>();
	
	private static LettersField instance = null;
	
	private LettersField() {
		//列データをプログラムで取り扱えるようもろもろ定義
		this.putColData(LETTER_ID,			"ID"				);
		this.putColData(VOTER_NAME,		"投票者氏名"		);
		this.putColData(VOTER_ADDRESS,	"投票者住所"		);
		this.putColData(VOTED_NAME,		"投票された名前"	);
		this.putColData(CHARA_ID,			"キャラクタ情報"	);
		this.putColData(CHARA_VARIATION,	"キャラ詳細"		);
		this.putColData(VOTED_NUM,			"書かれた票数"		);
		this.putColData(OPERATION,			"足し算/掛け算"		);
		this.putColData(VOTED_DATE,		"集計日時"			);
		this.putColData(REMARKS, 			"備考"				);
		
		//値が自動生成される列を定義。nullではINSERT/UPDATEに記述されない。
		//null以外では、固定的にクエリを記述する（CURRENT_TIMESTAMPとか）
		this.remarkAsAuto(_PKEY,		null);
	}
	
	public static LettersField getInstance() {
		if(instance == null) {
			instance = new LettersField();
		}
		return instance;
	}
	
	private void remarkAsAuto(String col, String value) {
		String[] exploded = col.split("\\.");
		String c = exploded[exploded.length - 1];
		autoColDat.put(c, value);
	}
	
	private void putColData(String col, String summary) {
		String[] exploded = col.split("\\.");
		String c;
		c = exploded[exploded.length - 1];
		columns.add(c);
		requestMap.put(c, c);
		summaryMap.put(c, summary);
	}
	
	@Override
	public String getTableName() {
		// TODO Auto-generated method stub
		return _TABLE;
	}

	@Override
	public String getIdColumn() {
		return this.getIdColumn(false);
	}

	@Override
	public String getIdColumn(boolean withTbl) {
		String[] exploded = _PKEY.split("\\.");
		String c = exploded[exploded.length - 1];
		return withTbl ? TSPECIFY + c : c;
	}
	
	@Override
	public String requestToColumn(String request) {
		return this.requestToColumn(request, false);
	}

	@Override
	public String requestToColumn(String request, boolean withTbl) {
		request = request.toLowerCase();
		String ret = this.requestMap.get(request);
		if(ret == null) {
			request = request.replaceAll(_TABLE + "\\.", "");
			ret = this.requestMap.get(request);
		}
		
		if(ret != null && withTbl) {
			ret = TSPECIFY + ret;
		}
		return ret;
	}
	
	@Override
	public Map<String, String> getAutoColumnData() {
		return this.getAutoColumnData(false);
	}

	@Override
	public Map<String, String> getAutoColumnData(boolean withTbl) {
		Map<String, String> retMap = new HashMap<String, String>();
		for(Entry<String, String>e: this.autoColDat.entrySet()) {
			String c = e.getKey();
			String v = e.getValue();
			if(withTbl) {
				c = TSPECIFY + c;
			}
			retMap.put(c, v);
		}
		return retMap;
	}
	
	@Override
	public List<String> getColumns(){
		return this.columns;
	}

}
