package info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.config;

import javax.sql.DataSource;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlite3.SQLitePlugin;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppJdbiConfig {
	@Bean("my-jdbi")
	public Jdbi jdbi(@Qualifier("my-sqlite") DataSource ds) {
		Jdbi jdbi = Jdbi.create(ds);
		jdbi.installPlugin(new SQLitePlugin());
		jdbi.installPlugin(new SqlObjectPlugin());
		return jdbi;
	}
}
