package info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas;

import java.math.BigInteger;

import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.LetterConstants;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.LetterEntity;

public class CharaEntity {
	private long		chara_id;		//キャラID。外部結合。
	private String		name;			//キャラ名。（例：竈門●治郎君）
	private String		came_from;		//出典。（第205死より）
	private BigInteger	voted_sum;		//投票数（元の数）。外部的には文字列として扱う。
	private BigInteger	voted_mul;		//×する係数（基本不使用。…不使用だよ？）
	private BigInteger res_abs;		//sum * mul。出す最終結果。ソートの都合上、絶対値で保存
	private int		res_positive;	//正の数か負の数か。
	private boolean	banned;			//亜空間行きのキャラかどうか。
	private String		remarks;		//備考
	
	public CharaEntity() {
		this.chara_id	= 0;
		this.banned		= false;
		this.remarks = "";
		this.clearResult();
	}
	
	
	
	//得票を逆回しで計算（削除、アップデート用）
	public void reverseCalculate(LetterEntity older) {
		switch(older.getOperation()) {
		case LetterConstants.OPE_MUL:
			this.voted_mul = this.voted_mul.divide(older.refVoted_num());
			break;
		default:
		case LetterConstants.OPE_ADD:
			this.voted_sum = this.voted_sum.subtract(older.refVoted_num());
			break;
		}
		
		this.updateResult();
	}
	
	//得票を加算する
	public void calculate(LetterEntity letter){
		switch(letter.getOperation()) {
		case LetterConstants.OPE_MUL:
			this.voted_mul = this.voted_mul.multiply(letter.refVoted_num());
			break;
		default:
		case LetterConstants.OPE_ADD:
			this.voted_sum = this.voted_sum.add(letter.refVoted_num());
			break;
		}
		
		this.updateResult();
	}
	
	//得票をリセット
	public void clearResult() {
		this.res_abs = new BigInteger("0");
		this.res_positive = 1;
		this.voted_mul = new BigInteger("1");
		this.voted_sum = new BigInteger("0");
	}
	
	private void updateResult() {
		this.res_abs = this.voted_sum.multiply(this.voted_mul);
		this.res_positive = this.res_abs.signum() >= 0 ? 1:0;
		this.res_abs = this.res_abs.abs();
	}
	
	//特殊なgetter/setter。DB処理/JSON用。
	public String getResult() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.res_positive > 0 ? "":"-").append(this.res_abs.toString());
		return sb.toString();
	}
	
	public String getVoted_sum() {
		return voted_sum.toString();
	}

	public void setVoted_sum(String voted_sum) {
		BigInteger tmpInt = new BigInteger(voted_sum);
		this.voted_sum = tmpInt;
	}
	
	public String getVoted_mul() {
		return voted_mul.toString();
	}
	
	public void setVoted_mul(String voted_mul) {
		this.voted_mul = new BigInteger(voted_mul);
	}
	
	public String getRes_abs() {
		return this.res_abs.toString();
	}
	
	public void setRes_abs(String res_abs) {
		this.res_abs = new BigInteger(res_abs).abs();
	}
	
	
	//以下、ボイラープレートのgetter/setter
	
	public long getChara_id() {
		return chara_id;
	}

	public void setChara_id(long chara_id) {
		this.chara_id = chara_id;
	}


	public boolean isBanned() {
		return banned;
	}

	public void setBanned(boolean banned) {
		this.banned = banned;
	}
	
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCame_from() {
		return came_from;
	}

	public void setCame_from(String came_from) {
		this.came_from = came_from;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRes_positive() {
		return this.res_positive;
	}

	public void setRes_positive(int res_positive) {
		this.res_positive = res_positive;
	}
	
}
