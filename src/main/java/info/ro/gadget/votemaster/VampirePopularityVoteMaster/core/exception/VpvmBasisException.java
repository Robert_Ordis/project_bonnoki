package info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.exception;

public class VpvmBasisException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected VpvmBasisException(String msg) {
		super(msg);
	}
	
	protected VpvmBasisException(String msg, Throwable e) {
		super(msg, e);
	}
	
	public static VpvmBasisException makeInstance(String msg) {
		return new VpvmBasisException(msg);
	}
	
	public static VpvmBasisException makeInstance(String msg, Throwable e) {
		return new VpvmBasisException(msg, e);
	}
	
}
