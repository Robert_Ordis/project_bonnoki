package info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.form;

import java.util.Set;

import org.springframework.data.util.Pair;

import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.exception.VpvmInvalidParamsException;

/**
 * Spring式でパラメータ→エンティティ生成を行うためのモノを定義したい。
 * @author Robert_Ordis
 *
 * @param <T>
 */
public interface EntityParamsPutter<T extends Object> {
	Pair<T, Set<String>> makeEntity(T raw) throws VpvmInvalidParamsException;
}
