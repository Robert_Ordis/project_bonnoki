package info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters;

public class LetterConstants {
	public static final int	OPE_ADD = 0;
	public static final int	OPE_MUL = 1;
	public static boolean inOperation(int val) {
		switch(val) {
		case OPE_ADD:
		case OPE_MUL:
			return true;
		}
		return false;
	}
}
