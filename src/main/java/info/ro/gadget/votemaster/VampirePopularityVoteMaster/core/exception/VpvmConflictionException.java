package info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class VpvmConflictionException extends VpvmBasisException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5572169273503827470L;
	
	protected VpvmConflictionException(String msg) {
		super(msg);
	}
	protected VpvmConflictionException(String msg, Throwable e) {
		super(msg, e);
	}
	
	public static VpvmConflictionException makeInstance(String resourceName, String subject, String value) {
		StringBuilder sb = new StringBuilder();
		sb.append(resourceName).append(":").append(subject).append(" -> CONFLICTED("+ value +")");
		return new VpvmConflictionException(sb.toString());
	}
}
