package info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.api;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import info.ro.gadget.jdbi_codes.JdbiHandleWrapper;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.CharaEntity;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.api.form.CharasForm;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.db.CharaEntityDao;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.db.CharaEntityRanker;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.db.CharasField;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.exception.VpvmBasisException;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.exception.VpvmConflictionException;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.exception.VpvmNotFoundException;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.LetterEntity;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.db.LetterEntityDao;

@Component
public class CharasService {
	@Autowired
	Jdbi jdbi;
	
	public List<CharaEntity> getRanking(boolean rejectSubspace, String search_col, String search_key){
		List<CharaEntity> ret = new LinkedList<CharaEntity>();
		CharaEntityRanker ranker;
		try(Handle h = jdbi.open()){
			ranker = new CharaEntityRanker(h, CharaEntity.class);
			if(rejectSubspace) {
				//亜空間キャラに関する事項。排除する時にそう指定する
				ranker.addWordCondition("subspace", CharasField.BANNED, "0", false, false);
			}
			ranker.addWordCondition("search", search_col, search_key);
			
			//1回目：正の数に絞って算出。絶対値の文字列長→中身の計算で高いところから並びだす。
			ranker.addWordCondition("sign", CharasField.RES_POSITIVE, "1", false, false);
			//ランキング処理は外部に任せることにしました。
			ranker.search((entity,  num)->{
				ret.add(entity);
				return true;
			}, false);
			
			//2回目：負の数で絞り込む。いったん設定した"sign"絞り込み設定を消去する
			ranker.clearSearchGroup("sign");
			//負の数から産出するので、絶対値の文字列長→中身の計算が低いところから並びだす
			ranker.addWordCondition("sign", CharasField.RES_POSITIVE, "0", false, false);
			ranker.setRankOrder("ASC");
			//ランキング処理は外部に任せることにしました。
			ranker.search((entity,  num)->{
				ret.add(entity);
				return true;
			}, false);
		}
		return ret;
	}
	
	public CharaEntity getById(String id) {
		CharaEntity ret = null;
		try {
			try(Handle h = jdbi.open()){
				CharaEntityDao dao = h.attach(CharaEntityDao.class);
				ret = dao.getById(Long.parseLong(id));
			}
		}catch(NumberFormatException e) {}
		return ret;
	}
	
	/**
	 * 入力値から、新しいエンティティを作成して取得する。
	 * @param form
	 * @return 新しいID
	 * @throws VpvmBasisException
	 */
	public long postFromForm(CharasForm form) throws VpvmBasisException {
		long ret;
		try(JdbiHandleWrapper h = new JdbiHandleWrapper(jdbi)){
			LetterEntityDao letterDao = h.getDao(LetterEntityDao.class);
			CharaEntityDao charaDao = h.getDao(CharaEntityDao.class);
			ret = h.inTransaction((handle)->{
				//1: 新規データを作成
				Pair<CharaEntity, Set<String>> p = form.makeEntity(null);
				CharaEntity newChara = p.getFirst();
				
				if(charaDao.getIdFromName(newChara.getName()) != null) {
					throw VpvmConflictionException.makeInstance("charas", "name", newChara.getName());
				}

				newChara.clearResult();

				//2: 新規データを置き、キャラIDを据える。
				long newId = charaDao.insert(newChara);
				newChara.setChara_id(newId);
				
				//3: もし仮に、該当IDが投票されていたら計算する
				boolean voted = false;
				for(LetterEntity l:letterDao.getCharasVote(newId)) {
					voted = true;
					newChara.calculate(l);
				}
				if(voted) {
					charaDao.updateCalculation(newChara);
				}
				return newId;
			});
		}
		return ret;
	}
	
	/**
	 * フォーム入力値から、更新後のエンティティを生成して記録する
	 * @param id
	 * @param src
	 * @return
	 * @throws VpvmBasisException
	 */
	public int putFromForm(String id, CharasForm src) throws VpvmBasisException {
		int ret = 0;
		try(JdbiHandleWrapper h = new JdbiHandleWrapper(jdbi)){
			LetterEntityDao letterDao = h.getDao(LetterEntityDao.class);
			CharaEntityDao charaDao = h.getDao(CharaEntityDao.class);
			
			ret = h.inTransaction((handle)->{
				//1: 対象のハガキを取得する。同時に更新後データも作成しておく
				CharaEntity c = null;
				CharaEntity newer = null;
				Pair<CharaEntity, Set<String>> p = null;
				try {
					c = charaDao.getById(Long.parseLong(id));
					p = src.makeEntity(c);
					newer = p.getFirst();
				}catch(NumberFormatException e) {}
				
				if(c == null) {
					throw VpvmNotFoundException.makeInstance("chara", id);
				}
				
				if(p.getSecond().size() <= 0) {
					//変更するものが何もない場合、そのまま終了。
					return 0;
				}
				
				Long detected = charaDao.getIdFromName(newer.getName());
				if(detected != null && detected != c.getChara_id()) {
					System.out.println("confliction");
					throw VpvmConflictionException.makeInstance("charas", "name", c.getName());
				}
				
				//2: 再集計指定をされていた場合、再集計。
				if(p.getSecond().contains(CharasField.RES_POSITIVE)) {
					System.out.println("re-calculate");
					//List<LetterEntity> voted = letterDao.getCharasVote(c.getChara_id());
					newer.clearResult();
					for(LetterEntity l:letterDao.getCharasVote(newer.getChara_id())) {
						newer.calculate(l);
					}
					
					System.out.printf("result: %s\n", newer.getResult());
					System.out.printf("inside length: %d\n", new BigInteger(newer.getResult()).bitLength());

				}
				
				//3: カラム指定とともにupdate
				return charaDao.partialUpdate(newer, p.getSecond());
			});
		}
		return ret;
	}

	public int deleteById(String id) throws VpvmBasisException{
		int ret = 0;
		try(JdbiHandleWrapper h = new JdbiHandleWrapper(jdbi)){
			LetterEntityDao letterDao = h.getDao(LetterEntityDao.class);
			CharaEntityDao charaDao = h.getDao(CharaEntityDao.class);
			
			ret = h.inTransaction((handle)->{
				//1: 対象のハガキを取得する。
				CharaEntity c = null;
				try {
					c = charaDao.getById(Long.parseLong(id));
				}catch(NumberFormatException e) {}
				
				if(c == null) {
					throw VpvmNotFoundException.makeInstance("chara", id);
				}
				
				//2: 消去処理。ハガキのほう消す。
				letterDao.deleteByCharaId(c.getChara_id());
				//3: キャラのほうも消す。
				return charaDao.delete(c.getChara_id());
			});
		}
		return ret;
	}

	public int bulkDelete(List<String> asList) {
		int ret = 0;
		try(JdbiHandleWrapper h = new JdbiHandleWrapper(jdbi)){
			LetterEntityDao letterDao = h.getDao(LetterEntityDao.class);
			CharaEntityDao charaDao = h.getDao(CharaEntityDao.class);
			
			ret = h.inTransaction((handle)->{
				int effected = 0;
				for(String id:asList) {
					try {
						long charaId = Long.parseLong(id);
						charaDao.delete(charaId);
						letterDao.deleteByCharaId(charaId);
						effected ++;
					}catch(NumberFormatException e) {}
				}
				return effected;
			});
		}
		return ret;
	}
	
	public int bulkSubspace(List<String> asList, boolean isBanned) {
		int ret = 0;
		try(JdbiHandleWrapper h = new JdbiHandleWrapper(jdbi)){
			CharaEntityDao charaDao = h.getDao(CharaEntityDao.class);
			
			ret = h.inTransaction((handle)->{
				if(asList == null || asList.size() == 0) {
					return 0;
				}
				return charaDao.bulkBan(asList, isBanned);
			});
		}
		return ret;
	}
	
	public Long getNearestIdFromName(String name, String method) {
		List<Pair<Double, CharaEntity>> pairs = new ArrayList<>();
		try(JdbiHandleWrapper h = new JdbiHandleWrapper(jdbi)){
			CharaEntityDao charaDao = h.getDao(CharaEntityDao.class);
			List<CharaEntity> charas;
			String keyword = name;
			keyword.replace("%", "[%]");
			keyword = "%" + keyword + "%";
			charas = charaDao.searchLikeName(keyword);
			if(charas == null || charas.size() <= 0) {
				charas = charaDao.getAll();
			}
			
			for(CharaEntity chara : charas) {
				if("Jaro".equalsIgnoreCase(method)) {
					pairs.add(Pair.of(StringUtils.getJaroWinklerDistance(chara.getName(), name), chara));
				}
				else {
					pairs.add(Pair.of((double)StringUtils.getLevenshteinDistance(chara.getName(),name), chara));
				}
			}
		}
		pairs.sort((a, b)->{
			double r = a.getFirst() - b.getFirst();
			if("Jaro".equals(method)) {
				r *= -1;
			}
			if(r < 0.0) {
				return -1;
			}
			else if(r > 0.0) {
				return 1;
			}
			return 0;
		});
		for(Pair<Double, CharaEntity> e : pairs) {
			CharaEntity c = e.getSecond();
			System.out.println(String.format("[%f] <- (%d:%s <-> %s)", e.getFirst(), c.getChara_id(), c.getName(), name));
		}
		return pairs.get(0).getSecond().getChara_id();
		
	}
}
