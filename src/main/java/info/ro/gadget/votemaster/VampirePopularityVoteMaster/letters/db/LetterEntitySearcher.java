package info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.db;

import org.jdbi.v3.core.Handle;

import com.ivanceras.fluent.sql.SQL;

import info.ro.gadget.jdbi_codes.field.DbFieldInfo;
import info.ro.gadget.jdbi_codes.search.AbstractSearcher;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.LetterEntity;

public class LetterEntitySearcher extends AbstractSearcher<LetterEntity>{
	
	DbFieldInfo fields = LettersField.getInstance();
	
	public LetterEntitySearcher(Handle handle, Class<LetterEntity> clazz) {
		super(handle, clazz);
	}

	@Override
	protected String getParentTableName() {
		// TODO Auto-generated method stub
		return fields.getTableName();
	}

	@Override
	protected String getSearchTableName() {
		// TODO Auto-generated method stub
		return fields.getTableName();
	}

	@Override
	protected String getPrimaryColumn() {
		// TODO Auto-generated method stub
		return fields.getIdColumn();
	}
	
	/**
	 * ここではソートはできては困るので、何もしない関数でオーバーライド。
	 */
	@Override
	public void setSortOrder(String column, String order) {
		
	}
	
	@Override
	protected void addDefaultSortCondition(SQL sql) {
		// TODO Auto-generated method stub
		sql = sql.FIELD(LettersField.LETTER_ID).keyword("DESC");
	}
	
	@Override
	protected String requestToColumn(String request) {
		// TODO Auto-generated method stub
		return fields.requestToColumn(request);
	}

	@Override
	protected boolean addExtraSearchCondition(SQL sql) {
		// TODO Auto-generated method stub
		return true;
	}
	
}
