package info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.exception;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
public class VpvmInvalidParamsException extends VpvmBasisException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5798615424168311222L;
	Map<String, String> errMap = new HashMap<String, String>();
	
	protected VpvmInvalidParamsException(String msg) {
		super(msg);
		// TODO Auto-generated constructor stub
	}
	
	public static VpvmInvalidParamsException makeInstance(String msg) {
		return new VpvmInvalidParamsException(msg);
	}
	
	public static VpvmInvalidParamsException makeInstance(Map<String, String> errMap) {
		VpvmInvalidParamsException th = new VpvmInvalidParamsException("");
		
		if(errMap != null) {
			for(Entry<String, String> e:errMap.entrySet()) {
				th.errMap.put(e.getKey(), e.getValue());
			}
		}
		
		return th;
	}
	
	public static VpvmInvalidParamsException makeInstance(VpvmInvalidParamsException thrown) {
		return makeInstance(thrown.errMap);
	}
	
	@Override
	public String getMessage() {
		if(this.errMap.size() <= 0) {
			return super.getMessage();
		}
		StringBuilder sb = new StringBuilder();
		for(Entry<String, String> e:this.errMap.entrySet()) {
			sb.append(e.getValue()).append(":").append(e.getKey()).append("\n");
		}
		return sb.toString();
	}
	
}
