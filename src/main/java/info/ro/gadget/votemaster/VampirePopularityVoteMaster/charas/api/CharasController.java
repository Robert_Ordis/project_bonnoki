package info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.api;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.CharaEntity;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.api.form.CharasForm;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.exception.VpvmBasisException;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.form.InputValid;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.form.PostValid;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.LetterEntity;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.api.LettersService;

@RestController
@RequestMapping("/charas")
@CrossOrigin
public class CharasController {
	@Autowired
	CharasService service;
	
	@Autowired
	LettersService letterService;
	
	@GetMapping({"/", ""})
	@ResponseBody
	public List<CharaEntity> getList(
			@RequestParam(defaultValue = "false") String reject_subspace,
			@RequestParam(defaultValue = "") String search_col,
			@RequestParam(defaultValue = "") String search_key){
		//System.out.println(params.get("keyword"));
		return service.getRanking(Boolean.parseBoolean(reject_subspace), search_col, search_key);
	}
	
	@GetMapping("/{id}")
	@ResponseBody
	public CharaEntity getById(@PathVariable("id") String id) throws VpvmBasisException {
		CharaEntity ret = service.getById(id);
		return ret;
	}
	
	@PostMapping({"/", ""})
	@ResponseBody
	public long postEntity(@Validated({PostValid.class, InputValid.class}) CharasForm form) throws VpvmBasisException {
		return service.postFromForm(form);
	}
	
	@PutMapping("/{id}")
	@ResponseBody
	public int putById(@PathVariable("id") String id, @Validated({InputValid.class}) CharasForm form) throws VpvmBasisException {
		return service.putFromForm(id, form);
	}
	
	@DeleteMapping("/{id}")
	@ResponseBody
	public int deleteById(@PathVariable("id") String id) throws VpvmBasisException {
		return service.deleteById(id);
	}
	
	@GetMapping("/{id}/letters")
	@ResponseBody
	public List<LetterEntity> getLettersOfChara(@PathVariable("id") String id, Long offset, Long limit) 
			throws VpvmBasisException{
		return letterService.getLettersOfChara(id, offset, limit);
	}
	
	@DeleteMapping({"/bulk-delete"})
	@ResponseBody
	public int bulkDelete(@RequestParam String[] chara_ids) throws VpvmBasisException {
		return service.bulkDelete(Arrays.asList(chara_ids));
	}
	
	@PutMapping("/bulk-subspace")
	@ResponseBody
	public int bulkSubspace(@RequestParam String[] chara_ids, @RequestParam Boolean subspace) throws VpvmBasisException {
		return service.bulkSubspace(Arrays.asList(chara_ids), subspace);
	}
	
	@GetMapping("/nearest")
	@ResponseBody
	public long nearestId(@RequestParam String name) throws VpvmBasisException{
		//System.out.println("一番近いのをさがす　");
		return service.getNearestIdFromName(name, "Jaro");
	}
}
