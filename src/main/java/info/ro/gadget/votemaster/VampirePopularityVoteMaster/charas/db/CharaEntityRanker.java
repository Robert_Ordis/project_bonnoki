package info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.db;

import org.jdbi.v3.core.Handle;

import com.ivanceras.fluent.sql.SQL;

import info.ro.gadget.jdbi_codes.field.DbFieldInfo;
import info.ro.gadget.jdbi_codes.search.AbstractSearcher;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.CharaEntity;

public class CharaEntityRanker extends AbstractSearcher<CharaEntity>{
	
	DbFieldInfo fields = CharasField.getInstance();
	private String rankOrder = "desc";
	
	public CharaEntityRanker(Handle handle, Class<CharaEntity> clazz) {
		super(handle, clazz);
	}

	@Override
	protected String getParentTableName() {
		// TODO Auto-generated method stub
		return fields.getTableName();
	}

	@Override
	protected String getSearchTableName() {
		// TODO Auto-generated method stub
		return fields.getTableName();
	}

	@Override
	protected String getPrimaryColumn() {
		// TODO Auto-generated method stub
		return fields.getIdColumn();
	}
	
	/**
	 * ここではソートはできては困るので、何もしない関数でオーバーライド。
	 */
	@Override
	public void setSortOrder(String column, String order) {
		
	}
	
	@Override
	protected void addDefaultSortCondition(SQL sql) {
		// TODO Auto-generated method stub
		//正の数かどうか -> 絶対値の長さ -> 文字列の中身でソートすれば概ねとらえらえるかな
		//order by res_positive desc, length(res_abs) desc, res_abs desc
		sql = sql.FIELD(CharasField.RES_POSITIVE).keyword(this.rankOrder);
		sql = sql.comma();
		sql = sql.FUNCTION("LENGTH", CharasField.RES_ABS).keyword(this.rankOrder);
		sql = sql.comma();
		sql = sql.FIELD(CharasField.RES_ABS).keyword(this.rankOrder);
	}
	
	@Override
	protected String requestToColumn(String request) {
		// TODO Auto-generated method stub
		return fields.requestToColumn(request);
	}

	@Override
	protected boolean addExtraSearchCondition(SQL sql) {
		// TODO Auto-generated method stub
		return true;
	}
	
	public void setRankOrder(String arg) {
		if(arg != null && arg.equalsIgnoreCase("desc")) {
			this.rankOrder = "DESC";
		}
		else {
			this.rankOrder = "ASC";
		}
	}
}
