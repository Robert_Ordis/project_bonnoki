package info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.db;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.customizer.BindList;
import org.jdbi.v3.sqlobject.customizer.Define;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.stringtemplate4.UseStringTemplateEngine;

import info.ro.gadget.jdbi_codes.dao.EntityInfoDao;
import info.ro.gadget.jdbi_codes.field.DbFieldInfo;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.CharaEntity;

@RegisterBeanMapper(CharaEntity.class)
public interface CharaEntityDao extends EntityInfoDao<CharaEntity>{
	final DbFieldInfo tblInfo = CharasField.getInstance();
	String tbl = CharasField._TABLE;
	String pky = CharasField._PKEY;
	
	@Override
	@SqlQuery("SELECT * FROM " + tbl)
	List<CharaEntity> getAll();

	@Override
	@SqlQuery("SELECT COUNT(*) FROM " + tbl)
	long countAll();

	@Override
	@SqlQuery("SELECT * FROM "+ tbl + " WHERE " + pky + " = :id")
	CharaEntity getById(@Bind("id") long ownId);

	@Override
	@SqlQuery("SELECT * FROM "+ tbl + " WHERE " + pky + " = :id FOR UPDATE")
	CharaEntity getByIdForUpdate(@Bind("id") long ownId);
	
	/**
	 * 名前→ID変換。重複は嫌よ。
	 * @param name
	 * @return
	 */
	@SqlQuery("SELECT "+ pky + " FROM " + tbl +" WHERE " + CharasField.NAME + " = :name")
	Long getIdFromName(@Bind("name") String name);
	
	@SqlQuery("SELECT * FROM " + tbl +" WHERE " + CharasField.NAME + " LIKE :name")
	List<CharaEntity> searchLikeName(@Bind("name") String name);
	//--------編集用クエリ--------
	@SqlUpdate("<query>")
	@GetGeneratedKeys(pky)
	@UseStringTemplateEngine
	long insert_(@Define("query") String query, @BindBean CharaEntity record);
	
	@Override
	default long partialInsert(CharaEntity record, Collection<String> cols) {
		String query = tblInfo.generatePartialInsert(cols);
		return query == null ? -1 : this.insert_(query, record);
	}
	
	@Override
	default long insert(CharaEntity record) {
		String query = tblInfo.generateInsert();
		return this.insert_(query, record);
	}
	
	@SqlUpdate("<query>")
	@UseStringTemplateEngine
	int update_(@Define("query") String query, @BindBean CharaEntity record);
	
	@Override
	default int partialUpdate(CharaEntity record, Collection<String> cols) {
		String query = tblInfo.generatePartialUpdate(cols);
		return query == null ? -1 : this.update_(query, record);
	}
	
	@Override
	default int update(CharaEntity record) {
		String query = tblInfo.generateUpdate();
		return this.update_(query, record);
	}
	
	/**
	 * 票数集計のみのアップデート
	 * @param record
	 * @return
	 */
	default int updateCalculation(CharaEntity record) {
		Set<String> cols = new HashSet<String>();
		cols.add(CharasField.RES_ABS);
		cols.add(CharasField.RES_POSITIVE);
		cols.add(CharasField.VOTED_MUL);
		cols.add(CharasField.VOTED_SUM);
		return this.partialUpdate(record, cols);
	}
	
	@Override
	@SqlUpdate("DELETE FROM " + tbl + " WHERE " + pky + " = :id")
	int delete(@Bind("id") long id);
	
	@SqlUpdate("DELETE FROM " + tbl + " WHERE " + pky + " IN (<ids>)")
	int bulkDelete(@BindList("ids")List<String> ids);
	/**
	 * 指定のIDを指定の数だけBANする
	 * @param ids
	 * @param isBanned
	 * @return
	 */
	default int bulkBan(List<String> ids, boolean isBanned) {
		return this.bulkBan_(ids, tblInfo.requestToColumn(CharasField.BANNED, false), isBanned);
	}
	@SqlUpdate(
		"UPDATE " + tbl + 
			" SET  <field> = :banned"+
			" WHERE " + pky + " IN (<ids>)"
	)
	int bulkBan_(@BindList("ids")List<String> ids, @Define("field") String field, @Bind("banned") boolean isBanned);
}
