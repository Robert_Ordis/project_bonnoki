package info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters;

import java.math.BigInteger;
import java.sql.Timestamp;


public class LetterEntity {
	private long		letter_id;			//ハガキID
	private String		voter_name;			//投票者の名前
	private String		voter_address;		//投票者の住所
	private String		voted_name;			//ハガキに書かれたキャラ名
	private long		chara_id;			//キャラクタ参照
	private String		chara_variation;	//キャラに関するちょっとした備考（○○した××など）
	private BigInteger	voted_num;			//集計数字。システム的にはマイナスもあり。
	private int		operation;			//0で足し算。1で掛け算。割り算は非対応。
	private Timestamp	voted_date;			//投票日時。一応ね。
	private String		remarks;
	
	public LetterEntity() {
		this.letter_id			= 0;
		this.voter_name			= "";
		this.voter_address		= "";
		this.voted_name			= "";
		this.chara_id			= 0;
		this.chara_variation	= "";
		this.voted_num			= new BigInteger("0");
		this.operation			= 0;
		this.voted_date			= new Timestamp(System.currentTimeMillis());
		this.setRemarks("");
	}
	
	public BigInteger refVoted_num() {
		return this.voted_num;
	}
	
	//BigIntegerとTimestampだけ表向きは普通の型として扱う。
	public String getVoted_num() {
		return voted_num.toString();
	}

	public void setVoted_num(String voted_num) {
		this.voted_num = new BigInteger(voted_num);
	}
	
	public long getVoted_date() {
		return this.voted_date.getTime();
	}
	
	public void setVoted_date(long voted_date) {
		this.voted_date = new Timestamp(voted_date);
	}
	
	//以下、ボイラープレート群
	public long getLetter_id() {
		return letter_id;
	}

	public void setLetter_id(long letter_id) {
		this.letter_id = letter_id;
	}

	public String getVoter_name() {
		return voter_name;
	}

	public void setVoter_name(String voter_name) {
		this.voter_name = voter_name;
	}

	public String getVoter_address() {
		return voter_address;
	}

	public void setVoter_address(String voter_address) {
		this.voter_address = voter_address;
	}

	public String getVoted_name() {
		return voted_name;
	}

	public void setVoted_name(String voted_name) {
		this.voted_name = voted_name;
	}

	public long getChara_id() {
		return chara_id;
	}

	public void setChara_id(long chara_id) {
		this.chara_id = chara_id;
	}

	public String getChara_variation() {
		return chara_variation;
	}

	public void setChara_variation(String chara_variation) {
		this.chara_variation = chara_variation;
	}

	public int getOperation() {
		return operation;
	}

	public void setOperation(int operation) {
		this.operation = operation;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
