package info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.api;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.result.ResultIterable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import info.ro.gadget.jdbi_codes.JdbiHandleWrapper;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.CharaEntity;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.db.CharaEntityDao;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.db.CharaEntityRanker;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.charas.db.CharasField;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.exception.VpvmBasisException;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.exception.VpvmInvalidParamsException;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.core.exception.VpvmNotFoundException;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.LetterEntity;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.api.form.LettersForm;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.db.LetterEntityDao;
import info.ro.gadget.votemaster.VampirePopularityVoteMaster.letters.db.LetterEntitySearcher;

@Component
public class LettersService {
	@Autowired
	Jdbi jdbi;
	
	List<LetterEntity> getAll(){
		List<LetterEntity> ret = null;
		try(Handle h = jdbi.open()){
			LetterEntityDao dao = h.attach(LetterEntityDao.class);
			ret = dao.getAll();
		}
		return ret;
	}
	
	LetterEntity getById(String id) throws VpvmNotFoundException {
		LetterEntity ret = null;
		try {
			try(JdbiHandleWrapper h = new JdbiHandleWrapper(jdbi)){
				LetterEntityDao dao = h.getDao(LetterEntityDao.class);
				ret = dao.getById(Long.parseLong(id));
			}
		}catch(NumberFormatException e) {}
		
		if(ret == null) {
			throw VpvmNotFoundException.makeInstance("letter", id);
		}
		return ret;
	}
	
	//long postFromMap(Map<String, String> src) throws VpvmInvalidParamsException {
	long postFromForm(LettersForm form) throws VpvmBasisException {
		long ret;
		try(JdbiHandleWrapper h = new JdbiHandleWrapper(jdbi)){
			LetterEntityDao letterDao = h.getDao(LetterEntityDao.class);
			CharaEntityDao charaDao = h.getDao(CharaEntityDao.class);
			ret = h.inTransaction((handle)->{
				//1: 新規データを作成
				//Pair<LetterEntity, Set<String>> madePair = putParams(null, src);
				Pair<LetterEntity, Set<String>> madePair = form.makeEntity(null);
				LetterEntity l = madePair.getFirst();
				
				//2: ハガキが投票したいキャラクターを探す。
				CharaEntity c = charaDao.getById(l.getChara_id());
				if(c == null) {
					//対象キャラ未定義ならエラーとする
					throw VpvmInvalidParamsException.makeInstance("Undefined character("+l.getChara_id()+")");
				}
				
				//3: 対象キャラの集計を更新する
				c.calculate(l);
				charaDao.updateCalculation(c);
				
				//4: letters更新
				return letterDao.insert(l);
			});
		}
		return ret;
	}
	
	//int putFromMap(String id, Map<String, String> src) throws VpvmBasisException {
	int putFromForm(String id, LettersForm form) throws VpvmBasisException {
		int ret = 0;
		try(JdbiHandleWrapper h = new JdbiHandleWrapper(jdbi)){
			LetterEntityDao letterDao = h.getDao(LetterEntityDao.class);
			CharaEntityDao charaDao = h.getDao(CharaEntityDao.class);
			
			ret = h.inTransaction((handle)->{
				//1: 対象のハガキを取得する。同時に更新後データも作成しておく
				LetterEntity l = null;
				Pair<LetterEntity, Set<String>> p = null;
				try {
					l = letterDao.getById(Long.parseLong(id));
					p = form.makeEntity(l);
				}catch(NumberFormatException e) {}
				
				if(l == null) {
					throw VpvmNotFoundException.makeInstance("letter", id);
				}
				
				if(p.getSecond().size() <= 0) {
					//変更するものが何もない場合、そのまま終了。
					return 0;
				}
				
				//2: 対象のキャラクターを取得し、集計を修正する
				CharaEntity c = charaDao.getById(l.getChara_id());
				if(c == null) {
					//throw VpvmBasisException.makeInstance("WARNING: Subject character("+l.getChara_id()+") was not found.");
					l = p.getFirst();
				}
				else {
					c.reverseCalculate(l);
					//3: 対象のハガキの修正後の値を取得し、計算しなおす
					l = p.getFirst();
					if(c.getChara_id() != l.getChara_id()) {
						//違うキャラに鞍替えするなら、ここでキャラテーブル書き込み
						charaDao.updateCalculation(c);
						c = charaDao.getById(l.getChara_id());
						if(c == null) {
							throw VpvmInvalidParamsException.makeInstance("Undefined character("+l.getChara_id()+")");
						}
					}
					
					c.calculate(l);
					charaDao.updateCalculation(c);
				}
				
				//4: 最後に、letterテーブル更新。
				return letterDao.partialUpdate(l, p.getSecond());
			});
			
		}
		return ret;
	}
	
	public List<LetterEntity> getLettersOfChara(String id, Long offset, Long limit) throws VpvmBasisException{
		List<LetterEntity> retList = new LinkedList<LetterEntity>();
		try(JdbiHandleWrapper h = new JdbiHandleWrapper(jdbi)){
			CharaEntity c = null;
			LetterEntityDao letterDao = h.getDao(LetterEntityDao.class);
			CharaEntityDao charaDao = h.getDao(CharaEntityDao.class);
			
			try {
				c = charaDao.getById(Long.parseLong(id));
			}catch(NumberFormatException e) {};
			
			if(c == null) {
				throw VpvmNotFoundException.makeInstance("charas", id);
			}
			if(offset == null) {
				offset = 0L;
			}
			if(limit == null) {
				limit = -1L;
			}
			if(limit < 0) {
				limit = letterDao.countAll();
			}
			//ResultIterable<LetterEntity> it = letterDao.getCharasVote(c.getChara_id());
			ResultIterable<LetterEntity> it = letterDao.getCharasVoteLimited(c.getChara_id(), limit, offset);
			for(LetterEntity l:it) {
				retList.add(l);
			}
		}
		return retList;
	}
	
	
	int deleteByid(String id) throws VpvmBasisException{
		int ret = 0;
		/*
		try(JdbiHandleWrapper h = new JdbiHandleWrapper(jdbi)){
			LetterEntityDao letterDao = h.getDao(LetterEntityDao.class);
			CharaEntityDao charaDao = h.getDao(CharaEntityDao.class);
			
			ret = h.inTransaction((handle)->{
				LetterEntity l = null;
				CharaEntity c = null;
				long letter_id = 0;
				try {
					letter_id = Long.parseLong(id);
					l = letterDao.getById(letter_id);
				}catch(NumberFormatException e) {}
				
				if(l == null) {
					throw VpvmNotFoundException.makeInstance("letter", id);
				}
				
				c = charaDao.getById(l.getChara_id());
				if(c != null) {
					c.reverseCalculate(l);
					charaDao.updateCalculation(c);
				}
				
				return letterDao.delete(letter_id);
			});
			
		}
		*/
		List<String> args = new LinkedList<String>();
		args.add(id);
		if(this.bulkDelete(args) <= 0) {
			throw VpvmNotFoundException.makeInstance("letter", id);
		}
		return 1;
	}

	public int bulkDelete(List<String> ids) {
		// TODO Auto-generated method stub
		int ret = 0;
		try(JdbiHandleWrapper h = new JdbiHandleWrapper(jdbi)){
			LetterEntityDao letterDao = h.getDao(LetterEntityDao.class);
			CharaEntityDao charaDao = h.getDao(CharaEntityDao.class);
			
			ret = h.inTransaction((handle)->{
				LetterEntity l = null;
				CharaEntity c = null;
				int effected = 0;
				for(String id:ids) {
					long letter_id = 0;
					try {
						letter_id = Long.parseLong(id);
						l = letterDao.getById(letter_id);
					}catch(NumberFormatException e) {
						continue;
					}
					
					if(l == null) {
						continue;
					}
					c = charaDao.getById(l.getChara_id());
					if(c != null) {
						c.reverseCalculate(l);
						charaDao.updateCalculation(c);
					}
					letterDao.delete(letter_id);
					effected++;
				}
				return effected;
			});
			
		}
		return ret;
	}

	public List<LetterEntity> getSearched(String search_col, String search_key) {
		List<LetterEntity> ret = new LinkedList<LetterEntity>();
		LetterEntitySearcher searcher;
		try(Handle h = jdbi.open()){
			searcher = new LetterEntitySearcher(h, LetterEntity.class);
			
			searcher.addWordCondition("search", search_col, search_key);
			
			//ランキング処理は外部に任せることにしました。
			searcher.search((entity,  num)->{
				ret.add(entity);
				return true;
			}, false);
			
		}
		return ret;
	}
	
	
}
