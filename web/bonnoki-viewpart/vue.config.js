module.exports = {
  pages: {
    index: {
      entry: 'src/pages/character-ranking/main.js', // エントリーポイントとなるjs
      template: 'public/index.html', // テンプレートのHTML
      filename: 'index.html', // build時に出力されるファイル名
      title: 'キャラクターランキング',
    },
    'character-ranking': {
      entry: 'src/pages/character-ranking/main.js', // エントリーポイントとなるjs
      template: 'public/index.html', // テンプレートのHTML
      filename: 'character-ranking.html', // build時に出力されるファイル名
      title: 'キャラクターランキング',
    },
    'character-modify': {
      entry: 'src/pages/character-modify/main.js', // エントリーポイントとなるjs
      template: 'public/index.html', // テンプレートのHTML
      filename: 'character-modify.html', // build時に出力されるファイル名
      title: 'キャラクター設定',
    },
    'vote-letters': {
      entry: 'src/pages/vote-letters/main.js',
      template: 'public/index.html',
      filename: 'vote-letters.html',
      title: '投票ハガキ一覧',
    },
    'batch-voting': {
      entry: 'src/pages/batch-voting/main.js',
      template: 'public/index.html',
      filename: 'batch-voting.html',
      title: '一挙集計',
    },
  },
  devServer: {
    port: 8888,
    disableHostCheck: true,
    proxy: 'http://localhost:8084'
    
  }
};
//編集後、vue inspectを実行。
