
import bigInt from 'big-integer'

const toHalfNum = function(s, tables){
  let reg = new RegExp('[' + tables + ']', 'g')
  return s.replace(reg, m => tables.indexOf(m))
}
const units = [
  '万', '億', '兆', '京', '垓', '𥝱', '穣', '溝',
  '澗', '正', '載', '極', '恒河沙', '阿僧祇', '那由多', '不可思議', '無量大数'
]
const lunits = ['十', '百', '千']
const wideNums  = "０１２３４５６７８９"
const kanjiNum1 = "〇一二三四五六七八九"
const kanjiNum2 = "零壱弐参肆伍陸漆捌玖"

//10^4区切りのマップ作製
var tmp = bigInt(1)
let munitMap = {}
let munitKeys
let munitReg
let lunitMap = {}
let lunitKeys
let lunitReg

for(var i = 0; i < units.length; i++){
  //tmp = tmp * 10000
  tmp = tmp.multiply(10000)
  munitMap[units[i]] = tmp
}

munitMap['秭']     = munitMap['𥝱']
munitMap['那由他'] = munitMap['那由多']
munitMap['萬']     = munitMap['万']
//正規表現で分割するとき用
munitKeys = Object.keys(munitMap).join("|")
munitReg = new RegExp('(' + munitKeys + ')')
//最後に*1を示すもの（空白）をマップに入れる→正規表現には入らない
munitMap[''] = bigInt(1)
munitMap['グーゴル'] = bigInt(10).pow(100)

//10^1区切りのマップ作製
tmp = bigInt(1)
for(i = 0; i < lunits.length; i++){
  //tmp = tmp * 10
  tmp = tmp.multiply(10)
  lunitMap[lunits[i]] = tmp
}
lunitMap['拾'] = lunitMap['什'] = lunitMap['十']
lunitMap['陌'] = lunitMap['佰'] = lunitMap['百']
lunitMap['阡'] = lunitMap['仟'] = lunitMap['千']
lunitKeys = Object.keys(lunitMap).join("|")
lunitReg = new RegExp('(' + lunitKeys + ')')
lunitMap[''] = bigInt(1)

export default function (val) {
  
  //漢字圏の命数法から逆にパースする
  
  //まずは区切りや漢数字、全角といったものを半角数字列に整形する。
  let tmpStr = val.replace(/[,.'\s]/gi, '')
  tmpStr = toHalfNum(tmpStr, wideNums)
  tmpStr = toHalfNum(tmpStr, kanjiNum1)
  tmpStr = toHalfNum(tmpStr, kanjiNum2)

  //符号
  let sign = tmpStr.charAt(0)
  if(["-", "－", "ー"].indexOf(sign) >= 0){
    sign = "-"
    tmpStr = tmpStr.substr(1)
  }
  else if(["+", "＋"].indexOf(sign) >= 0){
    sign = ""
    tmpStr = tmpStr.substr(1)
  }
  else{
    sign = ""
  }

  //無量大数(2)とかいう表記を出しているので、それの掃除
  tmpStr = tmpStr.replace(/\([^)]*\)/gi, '')
  
  let largestUnit = "無量大数"
  if(tmpStr.indexOf("グーゴル") >= 0){
    largestUnit = "グーゴル"
  }
  let resultInt = bigInt(0)
  //let tmpCalc = bigInt(0)
  //まずは無量大数で分割。
  //上から順に計算していき、次行くときに*10^64する。
  let chars = tmpStr.split(largestUnit)
  //console.log(munitKeys)
  //console.log(chars)
  for(let i = 0; i < chars.length; i++){
    //ループ1: 無量大数で分割。パートが進むごとに無量大数倍する。
    let partStr = chars[i]
    //console.log('partStr: ' + partStr)
    if(i == 0 && partStr.length == 0){
      //「最初のパート」で何も書かれていなかったら、一応1として扱う→「無量大数」単体対応
      resultInt = bigInt(1)
    }
    else{
      //ループ2: 億、万、兆などの万進単位で分割、計算。
      let middleParts = partStr.split(munitReg)
      //console.log("PARSE FOR MIDDLE")
      while(middleParts.length > 0){
        //splitの結果、[nnn, "単位", ...]のように分割される。
        let middleNum = middleParts.shift()
        let middleUnit = middleParts.shift()
        let middleInt = bigInt(0)
        //console.log("(M)num: [" + middleNum +"], uni: [" + middleUnit + "]")
        //取れない→空白＝倍を取らない。それとここで終わりのはず。
        middleUnit = (middleUnit == null || middleUnit == undefined) ? '':middleUnit
        if(middleNum == ''){
          //先頭無しでいきなり書かれたら、それは1を省略したものとする。
          middleInt = bigInt(middleUnit.length > 0 ? 1 : 0)
        }
        else{
          //ループ3: 千、百、十の考慮
          let littleParts = middleNum.split(lunitReg)
          //console.log("PARSE FOR LITTLE")
          while(littleParts.length > 0){
            let littleNum = littleParts.shift()
            let littleUnit = littleParts.shift()
            let littleInt
            //console.log("(L)num: [" + littleNum +"], uni: [" + littleUnit + "]")
            littleUnit = (littleUnit == null || littleUnit == undefined) ? '' : littleUnit
            littleInt = bigInt((littleUnit.length > 0) ? 1 : 0)
            if(littleNum.length > 0){
              if(isNaN(littleNum) && littleUnit.length > 0){
                //最末尾の場合、数字にパースできない文字は無視していい。
                return 'NaN'
              }
              littleInt = bigInt(littleNum.replace(/\D/g, ''))
            }
            littleInt = littleInt.multiply(lunitMap[littleUnit])
            middleInt = middleInt.add(littleInt)
          }
        }
        resultInt = resultInt.add(middleInt.multiply(munitMap[middleUnit]))
      }
    }
    if(i < (chars.length - 1)){
      resultInt = resultInt.multiply(munitMap[largestUnit])
    }
  }

  return sign + resultInt.toString()
}
