import {middleKanji, shortScale} from './numberNamer'

export default function (val, method) {
  //Split numeric string with specified len.
  //(123456, 4)->[3456, 12]->[12, 3456]
  let explodeByLen = function(val, len){
    let ret = []
    let count = 0
    let tmp = ''
    //console.log("explode "+val)
    for(let i = (val.length - 1); i >= 0; i--){
      tmp = val.charAt(i) + tmp
      count ++
      if(count == len){
        ret.push(tmp)
        tmp = ''
        count = 0
      }
    }
    if(tmp !== ''){
      ret.push(tmp)
    }

    let retRev = []
    for(let i = (ret.length - 1); i >= 0; i--){
      let n = parseInt(ret[i])
      if(retRev.length == 0 && n == 0){
        continue
      }
      retRev.push(ret[i])
    }
    //console.log(ret)
    return retRev;
  }
  
  //3桁分割
  let triBasedSeparation = function(val, separate){
    let tmp = explodeByLen(val, 3)
    let ret = ''
    //Ignore padded zeros at first of string.
    let firstZero = true
    for(let i = 0; i < tmp.length; i++){
      let n = parseInt(tmp[i])
      if(firstZero && n == 0){
        continue
      }

      let nStr = tmp[i]
      if(firstZero){
        nStr = n + ''
      }
      firstZero = false
      ret = ret + nStr
      if(i != (tmp.length - 1)){
        ret = ret + separate
      }
    }
    if(ret.length == 0){
      return '0'
    }
    return ret
  }
  
  //Represent with mechanically defined numeral system.
  let representWithNamer = function(val, namer){
    //Considering what unit must be append, I reverse splitted array at first.
    let unitCount = 0;
    let units = namer.units
    let tmp = explodeByLen(val, namer.digit).reverse()
    let ret = ''
    let loopCount = 1
    for(let i = 0; i < tmp.length; i++){
      let num = parseInt(tmp[i])
      if(i > 0){
        if(num != 0 || unitCount == (units.length - 1)){
          if(loopCount > 1 && unitCount == (units.length - 1)){
            ret = units[unitCount] + '(' +loopCount+ ')' + namer.appendix + ret
          }
          else{
            ret = units[unitCount] + namer.appendix + ret
          }
        }
        unitCount ++
        if(unitCount > (units.length - 1)){
          unitCount = 0
          loopCount ++;
        }
      }
      if(num != 0){
        ret = num + ret
      }
    }
    if(ret.length == 0){
      return '0'
    }
    return ret
  }
  
  if(val.length == 0){
    return '0'
  }
  let sign = val.charAt(0)
  if(sign == "-"){
    val = val.substr(1)
  }
  else{
    sign = ""
  }
  let ret = ''
  switch(method){
  case 'kanji':
    ret = representWithNamer(val, middleKanji)
    break
  case 'sscale':
    ret = representWithNamer(val, shortScale)
    break
  case 'space':
    ret = triBasedSeparation(val, ' ')
    break
  case 'quot':
    ret = triBasedSeparation(val, '\'')
    break
  case 'comma':
    ret = triBasedSeparation(val, ',')
    break
  case 'dot':
    ret = triBasedSeparation(val, '.')
    break
  case 'raw':
  default:
    ret = triBasedSeparation(val, '')
    break
  }
  return sign + ret
}
