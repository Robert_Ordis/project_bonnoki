import {middleKanji, littleKanji, shortScale, rawScale} from 'numberNamer'

export default function (val, method) {
  //数字を下の桁から決めた数に分割した表現で並べる
  let explodeByLen = function(val, len){
    let ret = []
    let count = 0
    let tmp = ''
    //console.log("explode "+val)
    for(let i = (val.length - 1); i >= 0; i--){
      tmp = val.charAt(i) + tmp
      count ++
      if(count == len){
        ret.push(tmp)
        tmp = ''
        count = 0
      }
    }
    if(tmp !== ''){
      ret.push(tmp)
    }

    let retRev = []
    for(let i = (ret.length - 1); i >= 0; i--){
      let n = parseInt(ret[i])
      if(retRev.length == 0 && n == 0){
        continue
      }
      retRev.push(ret[i])
    }
    //console.log(ret)
    return retRev;
  }
  //3桁分割
  let triBasedSeparation = function(val, separate){
    //漢字圏用に配列順がリバースされているのでさらにリバース
    let tmp = explodeByLen(val, 3)
    let ret = ''
    //最初にゼロがぐんぐん続いているようだとその時は書かない。
    let firstZero = true
    for(let i = 0; i < tmp.length; i++){
      let n = parseInt(tmp[i])
      if(firstZero && n == 0){
        continue
      }

      let nStr = tmp[i]
      if(firstZero){
        nStr = n + ''
      }
      firstZero = false
      ret = ret + nStr
      if(i != (tmp.length - 1)){
        ret = ret + separate
      }
    }
    if(ret.length == 0){
      return '0'
    }
    return ret
  }
  //漢字圏の命数法
  let representWithKanji = function(val){
    let units = [
      '万', '億', '兆', '京', '垓', '𥝱', '穣', '溝',
      '澗', '正', '載', '極', '恒河沙', '阿僧祇', '那由多', '不可思議', '無量大数'
    ]
    let unitCount = 0;
    let tmp = explodeByLen(val, 4).reverse()
    let ret = ''
    let loopCount = 1
    for(let i = 0; i < tmp.length; i++){
      let num = parseInt(tmp[i])
      if(i > 0){
        if(num != 0 || unitCount == (units.length - 1)){
          if(loopCount > 1 && unitCount == (units.length - 1)){
            ret = units[unitCount] + '(' +loopCount+ ')' + ret
          }
          else{
            ret = units[unitCount] + ret
          }
        }
        unitCount ++
        if(unitCount > (units.length - 1)){
          unitCount = 0
          loopCount ++;
        }
      }
      if(num != 0){
        ret = num + ret
      }
    }
    if(ret.length == 0){
      return '0'
    }
    return ret
  }
  if(val.length == 0){
    return '0'
  }
  let sign = val.charAt(0)
  if(sign == "-"){
    val = val.substr(1)
  }
  else{
    sign = ""
  }
  let ret = ''
  switch(method){
    case 'kanji':
      ret = representWithKanji(val)
      break
    case 'space':
      ret = triBasedSeparation(val, ' ')
      break
    case 'quot':
      ret = triBasedSeparation(val, '\'')
      break
    case 'comma':
      ret = triBasedSeparation(val, ',')
      break
    case 'dot':
      ret = triBasedSeparation(val, '.')
      break
    case 'raw':
    default:
      ret = triBasedSeparation(val, '')
      break
  }
  return sign + ret
}
