export {NumberPronounce}

let NumberPronounce = {
  'map': {},
  'reg': null
}

const basePron = ['ぜろ', 'いち', 'に', 'さん', 'し', 'ご', 'ろく', 'なな', 'はち', 'きゅう']
let pronMap = {}
for(var i = 0; i < basePron.length; i++){
	pronMap[basePron[i]] = i
}
//基数詞の「別読み」
pronMap['ひと'] = pronMap['いっ'] = pronMap['いち']
pronMap['ふた'] = pronMap['に']
pronMap['よん'] = pronMap['し']
pronMap['ろっ'] = pronMap['ろく']
pronMap['しち'] = pronMap['なな']
pronMap['はっ'] = pronMap['はち']
pronMap['く']   = pronMap['きゅう']
pronMap['れい'] = pronMap['まる'] = pronMap['ぜろ']

//基数詞につく単位
let otherProns = {
	'じゅう': '十', 'ひゃく': '百', 'せん': '千', 'まん' : '万', 'おく'  : '億', 
  'ちょう': '兆', 'けい'  : '京', 'がい': '垓', 'じょ' : '𥝱', 'じょう': '穣', 
  'こう'  : '溝', 'かん'  : '澗','せい' : '正', 'ざい' : '載', 'ごく'  : '極',
  'ごうがしゃ': '恒河沙',
  'あそうぎ'  : '阿僧祇', 
  'なゆた'    : '那由多', 
  'ふかしぎ'  : '不可思議',
  'むりょうたいすう': '無量大数'
} 

//単位の別読み
otherProns['じゅっ'] = otherProns['じっ']   = otherProns['じゅう']
otherProns['びゃく'] = otherProns['ぴゃく'] = otherProns['ひゃく']
otherProns['ぜん']   = otherProns['せん']

//マップの統合
Object.keys(otherProns).forEach(function(key){
	pronMap[key] = otherProns[key]
});
//カタカナの登録※半角ｶﾅまではお世話できないです。がぎござじぜびぴ
otherProns = {}
Object.keys(pronMap).forEach(function(key){
	var newKey = key.replace(/[\u3041-\u3096]/g, function(match) {
    var chr = match.charCodeAt(0) + 0x60;
    return String.fromCharCode(chr)
  });
  otherProns[newKey] = pronMap[key]
});
Object.keys(otherProns).forEach(function(newKey){
	pronMap[newKey] = otherProns[newKey]
})

//正規表現。その後これらを使って「よみ」→「数字or漢字」に変換する。
let pronReg = new RegExp(Object.keys(pronMap).join('|'), 'g')

NumberPronounce.map = pronMap
NumberPronounce.reg = pronReg

NumberPronounce.toNum = function(str){
    return str.replace(NumberPronounce.reg, m => NumberPronounce.map[m])
}
