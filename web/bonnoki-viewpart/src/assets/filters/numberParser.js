import {middleKanji, littleKanji, shortScale, rawScale} from './numberNamer'
import {NumberPronounce} from './numberPronounce'
import bigInt from 'big-integer'

const toHalfNum = function(s, tables){
  let reg = new RegExp('[' + tables + ']', 'g')
  return s.replace(reg, m => tables.indexOf(m))
}
const wideNums  = "０１２３４５６７８９"
const kanjiNum1 = "〇一二三四五六七八九"
const kanjiNum2 = "零壱弐参肆伍陸漆捌玖"

export default function (val) {
  //命数法から逆にパースする。
  
  let middleNamer = middleKanji
  let littleNamer = littleKanji
  let scReg = /.+llion|thousand/gi
  
  //まずは区切りや漢数字、全角といったものを半角数字列に整形する。
  let tmpStr = val.replace(/[,.'\s]/gi, '')
  tmpStr = NumberPronounce.toNum(tmpStr)
  tmpStr = toHalfNum(tmpStr, wideNums)
  tmpStr = toHalfNum(tmpStr, kanjiNum1)
  tmpStr = toHalfNum(tmpStr, kanjiNum2)
  
  //符号
  let sign = tmpStr.charAt(0)
  if(["-", "－", "ー"].indexOf(sign) >= 0){
    sign = "-"
    tmpStr = tmpStr.substr(1)
  }
  else if(["+", "＋"].indexOf(sign) >= 0){
    sign = ""
    tmpStr = tmpStr.substr(1)
  }
  else{
    sign = ""
  }
  
  //無量大数(2)とかいう表記を出しているので、それの掃除
  tmpStr = tmpStr.replace(/\([^)]*\)/gi, '')
  
  //thousandや～llionを検出したらshortscaleに切り替え。
  if(tmpStr.match(scReg) != null){
    middleNamer = shortScale
    littleNamer = rawScale
    console.log("Parse as short-scale")
  }
  
  let largestUnit = middleNamer.loopUnit
  if(middleNamer == middleKanji && tmpStr.indexOf('グーゴル') >= 0){
    largestUnit = 'グーゴル'
  }
  
  let resultInt = bigInt(0)
  let chars = tmpStr.split(largestUnit)
  let parsedCorrectly = true
  
  for(var i = 0; i < chars.length && parsedCorrectly; i++){
    let partStr = chars[i]
    if(i == 0 && partStr.length == 0){
      //Nothing is written in the "First part of looped num" -> treat as "1"
      //->For the case if string is written as "only the largest unit".
      resultInt = bigInt(1)
    }
    else{
      middleNamer.explodeToPart(partStr, function(middleNum, middleUnit, middleScale){
        let middleInt = bigInt(0)
        if(middleNum == ''){
          middleInt = bigInt(middleUnit.length > 0 ? 1: 0);
        }
        else {
          littleNamer.explodeToPart(middleNum, function(littleNum, littleUnit, littleScale){
            var littleInt = bigInt((littleUnit.length > 0) ? 1 : 0)
            if(littleNum.length > 0){
              if(isNaN(littleNum) && littleUnit.length > 0){
                //最末尾の場合、数字にパースできない文字は無視していい。
                parsedCorrectly = false
                return parsedCorrectly
              }
              littleInt = bigInt(littleNum.replace(/\D/g, ''))
            }
            //console.log("littleInt -> [" + littleInt.toString() + "]")
            littleInt = littleInt.multiply(littleScale)
            middleInt = middleInt.add(littleInt)
            //console.log("middleInt -> "+middleInt.toString())
            return parsedCorrectly
          })
        }
        
        resultInt = resultInt.add(middleInt.multiply(middleScale))
        //console.log("resultInt -> "+resultInt.toString())
        return parsedCorrectly;
      });
    }
    if(i < (chars.length - 1) && parsedCorrectly){
      resultInt = resultInt.multiply(middleNamer.map[largestUnit])
    }
  }
  
  return parsedCorrectly ? sign + resultInt.toString() : "NaN"
}
