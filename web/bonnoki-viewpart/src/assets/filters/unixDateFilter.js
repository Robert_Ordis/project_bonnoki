import moment from 'moment'

export default function (arg) {
  if(typeof(arg) != "number"){
    return "-"
  }
  var m = moment(arg)
  return m.format("YYYY-MM-DD HH:mm:ss")
}
