import bigInt from 'big-integer'

export {middleKanji, littleKanji, shortScale, rawScale}


var NumberNamer = function(units, digit){
  var tmp = bigInt(1)
  this.units = units
  this.scale = bigInt(10).pow(digit)
  this.digit = digit
  this.appendix = ''
  this.reg = null
  this.map = {}
  this.loopUnit = units[units.length - 1];
  
  for(var i = 0; i < units.length; i++){
    tmp = tmp.multiply(this.scale)
    this.map[units[i]] = tmp
    //console.log(units[i] + "["+i+"]->" + (tmp.toString().length - 1) + "/" + tmp.toString())
  }
  
};

/**
 *  \brief    Create number name parser based on mechanically defined numeral system.
 *  \arg    units:  list of words comming after normal number. e.g. million, 万, ...
 *  \arg    digit:  how many digit multiplies per unit.(radix 10)
 */
NumberNamer.prototype.refreshRegex = function(){
    var self = this
    var keys = Object.keys(self.map)
    if(keys.length >= 0){
        self.reg = new RegExp('(' + keys.join('|') + ')', 'i')
    }
    else{
        self.reg = null
    }
};

/**
 *  \brief    Add another represent unit.
    */
NumberNamer.prototype.addAlterRepresent = function(names, src){
    var self = this
    for(var i = 0; i < names.length; i++){
        self.map[names[i]] = self.map[src]
    }
};
  
/**
 *  \brief    Explode number string into each units.
    *  \arg    numStr        : string. structured with (\d+(unit)?)+。like 1億 or 10billion
    *  \arg    eachPartsCallback  : (numPart: str, uniPart: str, scale: bigint), return false if you want to stop.
    */
NumberNamer.prototype.explodeToPart = function(numstr, eachPartsCallback){
    var self = this
    
    if(self.reg == null){
        eachPartsCallback(numstr, '', bigInt(1))
        return
    }
    var parts = numstr.split(self.reg)
    //console.log(self.reg)
    //console.log("PARSE FOR "+self.units[0])
    //console.log(parts)
    while(parts.length > 0){
        let numPart = parts.shift()
        let uniPart = parts.shift()
        uniPart = (uniPart == null || uniPart == undefined) ? '':uniPart.toLowerCase()
        console.log("num["+numPart+"], uni["+uniPart+"], scale["+self.map[uniPart]+"]")
        if(!eachPartsCallback(numPart, uniPart, self.map[uniPart])){
            break
        }
    }
}

var middleKanji = new NumberNamer(
    [
        '万', '億', '兆', '京', '垓', '𥝱', '穣', '溝',
        '澗', '正', '載', '極', '恒河沙', '阿僧祇', '那由多', '不可思議', '無量大数'
    ],
    4
);

middleKanji.addAlterRepresent(['秭'], '𥝱')
middleKanji.addAlterRepresent(['那由他'], '那由多')
middleKanji.addAlterRepresent(['萬'], '万')
middleKanji.refreshRegex()

middleKanji.map['グーゴル'] = bigInt(10).pow(100)
middleKanji.map[''] = bigInt(1)

let littleKanji = new NumberNamer(
    ['十', '百', '千'],
    1
);
littleKanji.addAlterRepresent(['拾', '什'], '十')
littleKanji.addAlterRepresent(['陌', '佰'], '百')
littleKanji.addAlterRepresent(['阡', '仟'], '千')
littleKanji.refreshRegex()

littleKanji.map[''] = bigInt(1)

let shortScale = new NumberNamer(
    [
        //basic-llion
        'thousand', 'million', 'billion', 'trillion', 'quadrillion', 
        'quintillion', 'sextillion', 'septillion', 'octillion', 'nonillion',
        //decillion
        'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion', 
        'quindecillion', 'sexdecillion', 'septemdecillion', 'octodecillion', 'novemdecillion',
        //vigintillion
        'vigintillion', 'unvigintillion', 'duovigintillion', 'tresvigintillion', 'quattuorvigintillion',
        'quinvigintillion', 'sexvigintillion', 'septemvigintillion', 'octovigintillion', 'novemvigintillion',
        //trigintillion
        'trigintillion', 'untrigintillion', 'duotrigintillion', 'trestrigintillion', 'quattuortrigintillion', 
        'quintrigintillion', 'sextrigintillion', 'septemtrigintillion', 'octotrigintillion', 'novemtrigintillion',
        //quadragintillion
        'quadragintillion', 'unquadragintillion', 'duoquadragintillion', 'trequadragintillion', 'quattuorquadragintillion',
        'quinquadragintillion', 'sexquadragintillion', 'septemquadragintillion', 'octoquadragintillion', 'novemquadragintillion',
        //quinquagintillion
        'quinquagintillion', 'unquinquagintillion', 'duoquinquagintillion', 'trequinquagintillion', 'quattuorquinquagintillion', 
        'quinquinquagintillion', 'sexquinquagintillion', 'septemquinquagintillion', 'octoquinquagintillion', 'novemquinquagintillion',
        //sexagintillion
        'sexagintillion', 'unsexagintillion', 'duosexagintillion', 'tresexagintillion', 'quattuorsexagintillion',
        'quinsexagintillion', 'sexsexagintillion', 'septemsexagintillion', 'octosexagintillion', 'novemsexagintillion',
        //septuagintillion
        'septuagintillion', 'unseptuagintillion','duoseptuagintillion', 'treseptuagintillion', 'quattuorseptuagintillion',
        'quinseptuagintillion', 'sexseptuagintillion', 'septemseptuagintillion', 'octoseptuagintillion', 'novemseptuagintillion',
        //octogintillion
        'octogintillion', 'unoctogintillion', 'duooctogintillion', 'treoctogintillion', 'quattuoroctogintillion',
        'quinoctogintillion', 'sexoctogintillion', 'septemoctogintillion', 'octooctogintillion', 'novemoctogintillion',
        //nonagintillion
        'nonagintillion', 'unnonagintillion', 'duononagintillion', 'trenonagintillion', 'quattuornonagintillion',
        'quinnonagintillion', 'sexnonagintillion', 'septemnonagintillion', 'octononagintillion', 'novemnonagintillion', 
        //centillion
        'centillion'
    ],
    3
);

/// \note One of fuzzy input. "septem" <--> "septen", "novem" <--> "noven", "sex" <--> "ses"
var alterMap = {}
Object.keys(shortScale.map).forEach(function(key){
    var newKeys = [key]
    var replacement = {
        "sex": "ses",
        "septem": "septen",
        "novem": "noven",
    }
    
    Object.keys(replacement).forEach(function(keyword){
        var pushed = []
        newKeys.forEach(function(k){
            if(k.indexOf(keyword) >= 0){
                pushed.push(k.replace(keyword, replacement[keyword]))
            }
        })

        newKeys = newKeys.concat(pushed)
    })

    newKeys.shift()
    if(newKeys.length > 0){
        alterMap[key] = newKeys
    }
})

Object.keys(alterMap).forEach(function(key){
    shortScale.addAlterRepresent(alterMap[key], key)
})

shortScale.refreshRegex()
shortScale.map[''] = bigInt(1)
shortScale.appendix = ' '
let rawScale = new NumberNamer([], 1)


