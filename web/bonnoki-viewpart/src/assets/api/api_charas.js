var chara = '/charas/'

export default {
  charas: {
    list: function () { return chara },
    get: function (id) { return chara + id },
    letters: function (id) { return chara + id + '/letters/' },
    bulkDelete: function () { return chara + 'bulk-delete' },
    bulkSubspace: function () { return chara + 'bulk-subspace' },
  }
}
