var base = '/letters/'

export default {
  letters: {
    list: function () { return base },
    get: function (id) { return base + id },
    bulkDelete: function () {return base + 'bulk-delete'}
  }
}
