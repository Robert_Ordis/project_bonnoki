export default {
  ja: {
    page: {
      summary: 'ハガキ一覧',
      summary_col: '投票情報',
      unit: '票',
      column: {
        voted: '投票対象'
      }
    }
  },
  en: {
    page: {
      summary: 'Letters list',
      summary_col: 'Voted info',
      unit: 'votes',
      column: {
        voted: 'Voted Character'
      }
    }
  }
}
