export default {
  ja: {
    page: {
      summary: 'キャラクターランキング',
      num_of_charas: 'キャラクタ総数',
      total_votes: '合計',
      add_character: 'キャラクターを追加',
      unit: '票',
      bulk_subspace: 'まとめて亜空間に送る',
      bulk_inspace: 'まとめて亜空間から戻す',
      bulk_delete: 'まとめて消去する（戻せません）',
      uncheck_all: '全キャラのチェックを外す',
      friendship: '友情票パワーを算出する',
      auto_reload: '自動更新'
    }
  },
  en: {
    page: {
      summary: 'Character Ranking',
      num_of_charas: 'Number of characters',
      total_votes: 'Total voted',
      add_character: 'Add new character',
      unit: 'votes',
      bulk_subspace: 'Put checked characters into subspace',
      bulk_inspace: 'Reverse checked characters from subspace',
      bulk_delete: 'Delete checked characters(Cannot be undone)',
      uncheck_all: 'Uncheck all characters',
      friendship: 'Show the total of checked characters',
      auto_reload: 'Enable auto-reload'
    }
  }
}
