export default {
  ja: {
    popup: {
      processing: '処理中...',
      success   : '完了しました',
      error     : '失敗しました。作者のRobert_Ordisにお問い合わせください',
      beforeDelete: '消したものは戻せません。後悔しませんね？'
    }
  },
  en: {
    popup: {
      processing: 'Now processing...',
      success   : 'Complete successfully.',
      error     : 'Failure. Please contact to Robert_Ordis, the author of this app.',
      beforeDelete: 'If you delete once, I can never return them back. Will you never regret ?'
    }
  }
}
