export default {
  ja: {
    header: {
      title: '吸血鬼 人気投票の神',
      represent: {
        language: {
          name  : '言語',
          val  : {
            ja  : '日本語',
            en  : 'English'
          }
        },
        separation: {
          name: '数字の区切り',
          val: {
            kanji : '漢字圏',
            sscale: 'ショートスケール(-illion)',
            comma : 'コンマ区切り',
            dot   : 'ドット区切り',
            space : '空白区切り',
            quot  : 'アポストロフィ',
            raw   : '区切らない'
          }
        }
      },
      search: {
        keyword: 'キーワードを入力(順位は出てきたキャラ間のみ)',
        subspace: '亜空間のキャラを除外する',
        go: '検索'
      }
    }
  },
  en: {
    header: {
      title: 'Vampire: Popularity Vote Master',
      represent: {
        language: {
          name  : 'Language',
          val  : {
            ja  : '日本語',
            en  : 'English'
          }
        },
        separation: {
          name: 'Separation',
          val: {
            kanji : 'Kanji',
            sscale: 'ShortScale(-illion)',
            comma : 'Comma(,)',
            dot   : 'Dot(.)',
            space : 'Space',
            quot  : 'Quot(\')',
            raw   : 'NO'
          }
        }
      },
      search: {
        keyword: 'Input keyword (Rank will be calculated among the shown characters).',
        subspace: 'Reject subspace.',
        go: 'Search'
      }
    }
  }
}
