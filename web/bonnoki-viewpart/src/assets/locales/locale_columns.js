export default {
  ja: {
    columns: {
      charas: {
        chara_id    : "キャラクタID",
        rank        : "順位",
        name        : "キャラクタ名",
        came_from   : "出典",
        result      : "得票数",
        subspace    : "亜空間送り",
        remarks     : "備考",
        recalc      : "一から集計しなおす"
      },
      letters: {
        letter_id       : "ハガキID",
        voter_name      : "投票者名",
        voter_address   : "投票者住所",
        chara_variation : "キャラバリエーション",
        voted_name      : "書かれた名前",
        voted_num       : "書かれた票数",
        voted_date      : "投票日時",
        remarks         : "備考"
      }
    }
  },
  en: {
    columns: {
      charas: {
        chara_id    : "Chara ID",
        name        : "Name",
        rank        : "Rank",
        came_from   : "Appearance",
        result      : "Total Voted",
        subspace    : "Put into subspace",
        remarks     : "Remarks",
        recalc      : "Re-calculate now"
      },
      letters: {
        letter_id       : "Letter ID",
        voter_name      : "Voter Name",
        voter_address   : "Voter Address",
        chara_variation : "Variation",
        voted_name      : "Written Name",
        voted_num       : "Written Number",
        voted_date      : "Vote Date",
        remarks         : "Remarks"
      }
    }
  }
}
