export default {
  ja: {
    page: {
      summary: 'キャラクター編集',
      num_of_letters: 'ハガキ総数',
      unit: '票',
      not_found: '指定IDのキャラを取得できませんでした',
      letter_title: 'ハガキ一覧',
      post_letter: 'このキャラに投票する',
      delete_checked: 'チェックしたハガキを消去する（戻せません）',
      uncheck_all: '全ハガキのチェックを外す',
      current_total: '現在得票数',
    }
  },
  en: {
    page: {
      summary: 'Character Editing',
      num_of_letters: 'Number of posted letters',
      unit: 'votes',
      not_found: 'The character of specified ID was not found.',
      letter_title: 'Posted letters',
      post_letter: 'Vote a new letter to this character',
      delete_checked: 'Delete checked letters(Cannot be undone)',
      uncheck_all: 'Uncheck all letters',
      current_total: 'Currently Total Votes',
    }
  }
}
