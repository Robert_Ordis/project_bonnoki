import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    locale: 'ja'
  },
  mutations: {
    changeLocale ( state, lang ) {
      console.log("@ store: lang = " + lang)
      switch ( lang ) {
        case 'en':
          break
        case 'ja':
          break
        default:
          lang = 'ja'
      }
      state.locale = lang
    }
  }
})
