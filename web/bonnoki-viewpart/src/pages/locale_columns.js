export default {
  ja: {
    columns: {
      charas: {
        chara_id    : "キャラクタID",
        name        : "キャラクタ名",
        came_from   : "出典",
        result      : "得票数"
      },
      letters: {
        letter_id       : "ハガキID",
        voter_name      : "投票者名",
        voter_address   : "投票者住所",
        chara_variation : "キャラバリエーション",
        voted_name      : "書かれた名前",
        voted_num       : "書かれた票数",
        voted_date      : "投票日時",
        remarks         : "備考"
      }
    }
  },
  ja: {
    columns: {
      charas: {
        chara_id    : "Chara ID",
        name        : "Name",
        came_from   : "Appearance",
        result      : "Total Voted"
      },
      letters: {
        letter_id       : "Letter ID",
        voter_name      : "Voter Name",
        voter_address   : "Voter Address",
        chara_variation : "Variation",
        voted_name      : "Written Name",
        voted_num       : "Written Number",
        voted_date      : "Vote Date",
        remarks         : "Remarks"
      }
    }
  }
}
