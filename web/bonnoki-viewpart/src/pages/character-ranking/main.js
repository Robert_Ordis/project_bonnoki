import Vue from 'vue'
import App from './Top.vue'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import BSN from 'bootstrap.native/dist/bootstrap-native.esm.min.js'
import '@/assets/styles/bootstrap4-checkbox-switch.css'

import '@fortawesome/fontawesome-free/css/all.min.css'
import VueI18n from 'vue-i18n'
import store from '@/pages/stores.js'
import VModal from 'vue-js-modal'

require('popper.js/dist/umd/popper.min.js');

import '@/assets/styles/my-styles.css'

Vue.config.productionTip = false
Vue.use(BSN)
Vue.use(VueI18n)
Vue.use(VModal)

const i18n = new VueI18n({
  locale: 'ja',
  messages: {ja:{sample:'さんぷる'}, en:{sample:'sample'}}
})
new Vue({
  render: h => h(App),
  store,
  i18n,
  mounted() {
    BSN.initCallback()
  }
}).$mount('#app')
