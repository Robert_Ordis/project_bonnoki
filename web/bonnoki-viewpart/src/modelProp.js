import _ from 'lodash';

export default {
  props: {
    model: Object,
  },
  methods: {
    get (key) {
      return _.get(this.model, key);
    },
    set (key, value) {
      this.$emit(`update:model`, _.set(_.cloneDeep(this.model), key, value));
    },
  }
}
