# bonnoki-view

> view part of VampirePopulationVoteMaster

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## TODO

・デザインを決める。ページの構造はおおむね以下の3つ。
　１：トップ→集計ページも兼ねる。ただし、ソートするときはソートする。
　２：キャラクタごとのハガキ管理ページ。ここで追加したりとかもする。
　３：全体のハガキ検索ページ。変更なんかのダイアログはここでも呼べる。

・数の表示のフィルタ。欧米圏用に100,100,000,000式で表示するのと漢字圏用に1001億と表示するの両方を用意。

・数式からうまい具合に実際の数を計算できるライブラリとかないかな。

・結局、キャラ列挙APIはただ並べるだけになったので、順位付けはviewの方でやる。
　→並び順それそのものは数字順に並べられるので、そこは問題なし。

https://www.bravesoft.co.jp/blog/archives/2972
MPAの構築方法が載っているので使っちゃいましょう。

・vue-routerは使わないので、このプロジェクトも一度インストールしなおしです。
