AjaxQueue = function(){
	this.queue = [];
	this.processors = {
		limit	: 5,
		vector	: []
	};
	this.timeout = 5000;
	this.processed = 0;
};

(function(){
	//console.log("test")
	var start_process_ = function(ajaxQueue, request) {
		$.ajax({
			url: request.url,
			type: request.method,
			data: request.params,
			timeout: ajaxQueue.timeout,
			beforeSend: function(jqXHR, settings){
				ajaxQueue.processors.vector.push(jqXHR);
			},
			error: function(jqXHR, textStatus, errorThrown){
				//エラー→最後に入れなおす
				if(400 <= jqXHR.status && jqXHR.status <= 499){
					//クライアントが悪い案件なので、カバーしない
					request.onAbandon(jqXHR, textStatus, errorThrown);
				}
				else{
					ajaxQueue.queue.push(request);
				}
			},
			success: function(data, textStatus, jqXHR){
				//成功→何をする？
				//console.log(data);
				request.onSuccess(data);
				ajaxQueue.processed ++;
			},
			complete: function(jqXHR, textStatus){
				
				//終わったら、とりあえずプロセッサを消去する
				var idx = ajaxQueue.processors.vector.indexOf(jqXHR);
				//console.log("idx was "+idx);
				if(idx >= 0){
					ajaxQueue.processors.vector.splice(idx, 1);
				}
				//console.log("proc len is "+ajaxQueue.processors.vector.length);
				//そのうえで、プロセッサに空きができたらまた処理を起動する
				if(ajaxQueue.processors.vector.length < ajaxQueue.processors.limit){
					ajaxQueue.dequeRequest();
				}
			}
		});
	};
	
	AjaxQueue.prototype.pushRequest = function(method, url, params, onSuccess, onAbandon){
		//プロセッサ数がlimit未満→そのままajaxを作る。
		//プロセッサ数がlimit以上→キューに入れる。
		var self = this;
		//console.log(self);
		if(onSuccess == null || onSuccess == undefined){
			onSuccess = function(dat){};
		}
		if(onAbandon == null || onAbandon == undefined){
			onAbandon = function(jqXHR, textStatus, errorThrown){};
		}
		var request = {
			url: url,
			method: method,
			params: params,
			onSuccess: onSuccess,
			onAbandon: onAbandon
		};
		self.queue.push(request);
		if(self.processors.vector.length < self.processors.limit){
			self.dequeRequest();
		}
	};
	
	AjaxQueue.prototype.dequeRequest = function(){
		//何か一つリクエストを取り出す
		var self = this;
		//console.log("queue remaining is "+self.queue.length);
		//console.log("processors len is "+self.processors.vector.length);
		var request = self.queue.shift();
		if(request == null || request == undefined){
			return;
		}
		start_process_(self, request);
	};
	
})();
