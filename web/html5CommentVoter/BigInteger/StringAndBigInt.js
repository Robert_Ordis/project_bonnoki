var numberParser;
var numberFilter;

/**
 *	\brief		Create number name parser based on mechanically defined numeral system.
 *	\arg		units:	list of INITIAL words comming after normal number. e.g. million, 万, ...
 *	\arg		digit:	how many digit multiplies per unit.(radix 10)
 *	\remarks	units is also used for "numberFilter"
 */
var NumberNamer = function(units, digit){
	var tmp = bigInt(1)
	this.units = units
	this.scale = bigInt(10).pow(digit)
	this.digit = digit
	this.appendix = ''
	this.reg = null
	this.map = {}
	this.loopUnit = units[units.length - 1];
	
	for(var i = 0; i < units.length; i++){
		tmp = tmp.multiply(this.scale)
		this.map[units[i]] = tmp
	}
	
};

(function(){
	
	/**
	 *	\brief		Create number name parser based on mechanically defined numeral system.
	 *	\arg		units:	list of words comming after normal number. e.g. million, 万, ...
	 *	\arg		digit:	how many digit multiplies per unit.(radix 10)
	 */
	NumberNamer.prototype.refreshRegex = function(){
		var self = this
		var keys = Object.keys(self.map)
		if(keys.length >= 0){
			self.reg = new RegExp('(' + keys.join('|') + ')', 'i')
		}
		else{
			self.reg = null
		}
	};
	
	/**
	 *	\brief		Add another represent unit.
	 */
	NumberNamer.prototype.addAlterRepresent = function(names, src){
		var self = this
		for(var i = 0; i < names.length; i++){
			this.map[names[i]] = this.map[src]
		}
	};
	
	/**
	 *	\brief		Explode number string into each units.
	 *	\arg		numStr				: string. structured with (\d+(unit)?)+。like 1億 or 10billion
	 *	\arg		eachPartsCallback	: (numPart: str, uniPart: str, scale: bigint), return false if you want to stop.
	 */
	NumberNamer.prototype.explodeToPart = function(numstr, eachPartsCallback){
		var self = this
		
		if(self.reg == null){
			eachPartsCallback(numstr, '', bigInt(1))
			return
		}
		var parts = numstr.split(self.reg)
		//console.log(self.reg)
		//console.log("PARSE FOR "+self.units[0])
		//console.log(parts)
		while(parts.length > 0){
			let numPart = parts.shift()
			let uniPart = parts.shift()
			uniPart = (uniPart == null || uniPart == undefined) ? '':uniPart.toLowerCase()
			console.log("num["+numPart+"], uni["+uniPart+"], scale["+self.map[uniPart]+"]")
			if(!eachPartsCallback(numPart, uniPart, self.map[uniPart])){
				break
			}
		}
	}
	
})();

var NumberPronounce = {
  'map': {},
  'reg': null,
  'toNum': null
};

(function(){

const basePron = ['ぜろ', 'いち', 'に', 'さん', 'し', 'ご', 'ろく', 'なな', 'はち', 'きゅう']
let pronMap = {}
for(var i = 0; i < basePron.length; i++){
	pronMap[basePron[i]] = i
}
//基数詞の「別読み」
pronMap['ひと'] = pronMap['いっ'] = pronMap['いち']
pronMap['ふた'] = pronMap['に']
pronMap['よん'] = pronMap['し']
pronMap['ろっ'] = pronMap['ろく']
pronMap['しち'] = pronMap['なな']
pronMap['はっ'] = pronMap['はち']
pronMap['く']   = pronMap['きゅう']
pronMap['れい'] = pronMap['まる'] = pronMap['ぜろ']

//基数詞につく単位
let otherProns = {
	'じゅう': '十', 'ひゃく': '百', 'せん': '千', 'まん' : '万', 'おく'  : '億', 
	'ちょう': '兆', 'けい'  : '京', 'がい': '垓', 'じょ' : '𥝱', 'じょう': '穣', 
	'こう'  : '溝', 'かん'  : '澗','せい' : '正', 'ざい' : '載', 'ごく'  : '極',
	'ごうがしゃ': '恒河沙',
	'あそうぎ'  : '阿僧祇', 
	'なゆた'    : '那由多', 
	'ふかしぎ'  : '不可思議',
	'むりょうたいすう': '無量大数'
}

//単位の別読み
otherProns['じゅっ'] = otherProns['じっ']   = otherProns['じゅう']
otherProns['びゃく'] = otherProns['ぴゃく'] = otherProns['ひゃく']
otherProns['ぜん']   = otherProns['せん']

//マップの統合
Object.keys(otherProns).forEach(function(key){
	pronMap[key] = otherProns[key]
});
//カタカナの登録※半角ｶﾅまではお世話できないです。がぎござじぜびぴ
otherProns = {}
Object.keys(pronMap).forEach(function(key){
		var newKey = key.replace(/[\u3041-\u3096]/g, function(match) {
			var chr = match.charCodeAt(0) + 0x60;
			return String.fromCharCode(chr)
		});
	otherProns[newKey] = pronMap[key]
});
Object.keys(otherProns).forEach(function(newKey){
	pronMap[newKey] = otherProns[newKey]
})

//正規表現。その後これらを使って「よみ」→「数字or漢字」に変換する。
var pronReg = new RegExp(Object.keys(pronMap).join('|'), 'g')

NumberPronounce.map = pronMap
NumberPronounce.reg = pronReg

NumberPronounce.toNum = function(str){
	return str.replace(NumberPronounce.reg, m => NumberPronounce.map[m])
}


})();

(function(){


const toHalfNum = function(s, tables){
	let reg = new RegExp('[' + tables + ']', 'g')
	return s.replace(reg, m => tables.indexOf(m))
}

const wideNums  = "０１２３４５６７８９"
const kanjiNum1 = "〇一二三四五六七八九"
const kanjiNum2 = "零壱弐参肆伍陸漆捌玖"


let middleKanji = new NumberNamer(
	[
		'万', '億', '兆', '京', '垓', '𥝱', '穣', '溝',
		'澗', '正', '載', '極', '恒河沙', '阿僧祇', '那由多', '不可思議', '無量大数'
	],
	4
);
middleKanji.addAlterRepresent(['秭'], '𥝱')
middleKanji.addAlterRepresent(['那由他'], '那由多')
middleKanji.addAlterRepresent(['萬'], '万')
middleKanji.refreshRegex()

middleKanji.map['グーゴル'] = bigInt(10).pow(100)
middleKanji.map[''] = bigInt(1)

let littleKanji = new NumberNamer(
	['十', '百', '千'],
	1
);
littleKanji.addAlterRepresent(['拾', '什'], '十')
littleKanji.addAlterRepresent(['陌', '佰'], '百')
littleKanji.addAlterRepresent(['阡', '仟'], '千')
littleKanji.refreshRegex()

littleKanji.map[''] = bigInt(1)

let shortScale = new NumberNamer(
	[
		//basic-llion
		'thousand', 'million', 'billion', 'trillion', 'quadrillion', 
		'quintillion', 'sextillion', 'septillion', 'octillion', 'nonillion',
		//decillion
		'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion', 
		'quindecillion', 'sexdecillion', 'septemdecillion', 'octodecillion', 'novemdecillion',
		//vigintillion
		'vigintillion', 'unvigintillion', 'duovigintillion', 'tresvigintillion', 'quattuorvigintillion',
		'quinvigintillion', 'sexvigintillion', 'septemvigintillion', 'octovigintillion', 'novemvigintillion',
		//trigintillion
		'trigintillion', 'untrigintillion', 'duotrigintillion', 'trestrigintillion', 'quattuortrigintillion', 
		'quintrigintillion', 'sextrigintillion', 'septemtrigintillion', 'octotrigintillion', 'novemtrigintillion',
		//quadragintillion
		'quadragintillion', 'unquadragintillion', 'duoquadragintillion', 'trequadragintillion', 'quattuorquadragintillion',
		'quinquadragintillion', 'sexquadragintillion', 'septemquadragintillion', 'octoquadragintillion', 'novemquadragintillion',
		//quinquagintillion
		'quinquagintillion', 'unquinquagintillion', 'duoquinquagintillion', 'trequinquagintillion', 'quattuorquinquagintillion', 
		'quinquinquagintillion', 'sexquinquagintillion', 'septemquinquagintillion', 'octoquinquagintillion', 'novemquinquagintillion',
		//sexagintillion
		'sexagintillion', 'unsexagintillion', 'duosexagintillion', 'tresexagintillion', 'quattuorsexagintillion',
		'quinsexagintillion', 'sexsexagintillion', 'septemsexagintillion', 'octosexagintillion', 'novemsexagintillion',
		//septuagintillion
		'septuagintillion', 'unseptuagintillion','duoseptuagintillion', 'treseptuagintillion', 'quattuorseptuagintillion',
		'quinseptuagintillion', 'sexseptuagintillion', 'septemseptuagintillion', 'octoseptuagintillion', 'novemseptuagintillion',
		//octogintillion
		'octogintillion', 'unoctogintillion', 'duooctogintillion', 'treoctogintillion', 'quattuoroctogintillion',
		'quinoctogintillion', 'sexoctogintillion', 'septemoctogintillion', 'octooctogintillion', 'novemoctogintillion',
		//nonagintillion
		'nonagintillion', 'unnonagintillion', 'duononagintillion', 'trenonagintillion', 'quattuornonagintillion',
		'quinnonagintillion', 'sexnonagintillion', 'septemnonagintillion', 'octononagintillion', 'novemnonagintillion', 
		//centillion
		'centillion'
	],
	3
);
/// \note One of fuzzy input. "septem" <--> "septen", "novem" <--> "noven", "sex" <--> "ses"
var alterMap = {}
Object.keys(shortScale.map).forEach(function(key){
	var newKeys = [key]
	var replacement = {
		"sex": "ses",
		"septem": "septen",
		"novem": "noven",
	}
	
	Object.keys(replacement).forEach(function(keyword){
		var pushed = []
		newKeys.forEach(function(k){
			if(k.indexOf(keyword) >= 0){
				pushed.push(k.replace(keyword, replacement[keyword]))
			}
		})
		
		newKeys = newKeys.concat(pushed)
	})
	
	newKeys.shift()
	if(newKeys.length > 0){
		alterMap[key] = newKeys
	}
})

Object.keys(alterMap).forEach(function(key){
    shortScale.addAlterRepresent(alterMap[key], key)
})

shortScale.refreshRegex()
shortScale.map[''] = bigInt(1)
shortScale.appendix = ' '
let rawScale = new NumberNamer([], 1)

numberParser = function(val){
	//命数法から逆にパースする。
	
	let middleNamer = middleKanji
	let littleNamer = littleKanji
	let scReg = /.+llion|thousand/gi
	
	//まずは区切りや漢数字、全角といったものを半角数字列に整形する。
	let tmpStr = val.replace(/[,.'\s]/gi, '')
	tmpStr = NumberPronounce.toNum(tmpStr)
	tmpStr = toHalfNum(tmpStr, wideNums)
	tmpStr = toHalfNum(tmpStr, kanjiNum1)
	tmpStr = toHalfNum(tmpStr, kanjiNum2)
	
	//符号
	let sign = tmpStr.charAt(0)
	if(["-", "－", "ー"].indexOf(sign) >= 0){
		sign = "-"
		tmpStr = tmpStr.substr(1)
	}
	else if(["+", "＋"].indexOf(sign) >= 0){
		sign = ""
		tmpStr = tmpStr.substr(1)
	}
	else{
		sign = ""
	}
	
	//無量大数(2)とかいう表記を出しているので、それの掃除
	tmpStr = tmpStr.replace(/\([^)]*\)/gi, '')
	
	//thousandや～llionを検出したらshortscaleに切り替え。
	if(tmpStr.match(scReg) != null){
		middleNamer = shortScale
		littleNamer = rawScale
		console.log("Parse as short-scale")
	}
	
	let largestUnit = middleNamer.loopUnit
	if(middleNamer == middleKanji && tmpStr.indexOf('グーゴル') >= 0){
		largestUnit = 'グーゴル'
	}
	
	let resultInt = bigInt(0)
	let chars = tmpStr.split(largestUnit)
	let parsedCorrectly = true
	
	for(var i = 0; i < chars.length && parsedCorrectly; i++){
		let partStr = chars[i]
		if(i == 0 && partStr.length == 0){
			//Nothing is written in the "First part of looped num" -> treat as "1"
			//->For the case if string is written as "only the largest unit".
			resultInt = bigInt(1)
		}
		else{
			middleNamer.explodeToPart(partStr, function(middleNum, middleUnit, middleScale){
				let middleInt = bigInt(0)
				if(middleNum == ''){
					middleInt = bigInt(middleUnit.length > 0 ? 1: 0);
				}
				else {
					littleNamer.explodeToPart(middleNum, function(littleNum, littleUnit, littleScale){
						var littleInt = bigInt((littleUnit.length > 0) ? 1 : 0)
						if(littleNum.length > 0){
							if(isNaN(littleNum) && littleUnit.length > 0){
								//最末尾の場合、数字にパースできない文字は無視していい。
								parsedCorrectly = false
								return parsedCorrectly
							}
							littleInt = bigInt(littleNum.replace(/\D/g, ''))
						}
						//console.log("littleInt -> [" + littleInt.toString() + "]")
						littleInt = littleInt.multiply(littleScale)
						middleInt = middleInt.add(littleInt)
						//console.log("middleInt -> "+middleInt.toString())
						return parsedCorrectly
					})
				}
				
				resultInt = resultInt.add(middleInt.multiply(middleScale))
				//console.log("resultInt -> "+resultInt.toString())
				return parsedCorrectly;
			});
		}
		if(i < (chars.length - 1) && parsedCorrectly){
			resultInt = resultInt.multiply(middleNamer.map[largestUnit])
		}
	}
	
	return parsedCorrectly ? sign + resultInt.toString() : "NaN"
}

numberFilter = function (val, method) {
	//Split numeric string with specified len.
	//(123456, 4)->[3456, 12]->[12, 3456]
	let explodeByLen = function(val, len){
		let ret = []
		let count = 0
		let tmp = ''
		//console.log("explode "+val)
		for(let i = (val.length - 1); i >= 0; i--){
			tmp = val.charAt(i) + tmp
			count ++
			if(count == len){
				ret.push(tmp)
				tmp = ''
				count = 0
			}
		}
		if(tmp !== ''){
			ret.push(tmp)
		}

		let retRev = []
		for(let i = (ret.length - 1); i >= 0; i--){
			let n = parseInt(ret[i])
			if(retRev.length == 0 && n == 0){
				continue
			}
			retRev.push(ret[i])
		}
		//console.log(ret)
		return retRev;
	}
	
	//3桁分割
	let triBasedSeparation = function(val, separate){
		let tmp = explodeByLen(val, 3)
		let ret = ''
		//Ignore padded zeros at first of string.
		let firstZero = true
		for(let i = 0; i < tmp.length; i++){
			let n = parseInt(tmp[i])
			if(firstZero && n == 0){
				continue
			}

			let nStr = tmp[i]
			if(firstZero){
				nStr = n + ''
			}
			firstZero = false
			ret = ret + nStr
			if(i != (tmp.length - 1)){
				ret = ret + separate
			}
		}
		if(ret.length == 0){
			return '0'
		}
		return ret
	}
	
	//Represent with mechanically defined numeral system.
	let representWithNamer = function(val, namer){
		//Considering what unit must be append, I reverse splitted array at first.
		let unitCount = 0;
		let units = namer.units
		let tmp = explodeByLen(val, namer.digit).reverse()
		let ret = ''
		let loopCount = 1
		for(let i = 0; i < tmp.length; i++){
			let num = parseInt(tmp[i])
			if(i > 0){
				if(num != 0 || unitCount == (units.length - 1)){
					if(loopCount > 1 && unitCount == (units.length - 1)){
						ret = units[unitCount] + '(' +loopCount+ ')' + namer.appendix + ret
					}
					else{
						ret = units[unitCount] + namer.appendix + ret
					}
				}
				unitCount ++
				if(unitCount > (units.length - 1)){
					unitCount = 0
					loopCount ++;
				}
			}
			if(num != 0){
				ret = num + ret
			}
		}
		if(ret.length == 0){
			return '0'
		}
		return ret
	}
	
	if(val.length == 0){
		return '0'
	}
	let sign = val.charAt(0)
	if(sign == "-"){
		val = val.substr(1)
	}
	else{
		sign = ""
	}
	let ret = ''
	switch(method){
	case 'kanji':
		ret = representWithNamer(val, middleKanji)
		break
	case 'short-scale':
		ret = representWithNamer(val, shortScale)
		break
	case 'space':
		ret = triBasedSeparation(val, ' ')
		break
	case 'quot':
		ret = triBasedSeparation(val, '\'')
		break
	case 'comma':
		ret = triBasedSeparation(val, ',')
		break
	case 'dot':
		ret = triBasedSeparation(val, '.')
		break
	case 'raw':
	default:
		ret = triBasedSeparation(val, '')
		break
	}
	return sign + ret
}



})();
