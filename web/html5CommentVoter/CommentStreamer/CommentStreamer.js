/**
 *	\brief		読み込んだxmlの内、"comment"ノードにあるものを取り扱う。
 *	\remarks	コメントが遅れて記述された場合、最新から60[sec]の分を許容する。
 */
CommentStreamer = function(initTime){
	var self = this;
	this.startTime = parseInt(initTime);
	this.border = -60 * 1000;
	this.onTreatCommentCallback = function(nodeNum, xmlNode){};
	this.laterRead = {}	//timestamp -> [set of no.]
	this.isFirst = (self.startTime >= 0);	//マイナスを投げられたらデバッグ用。全部一度読む。
	setInterval(function(){
		console.log("cleaning...");
		var borderTime = self.startTime + self.border;
		for(var key in Object.keys(self.laterRead)){
			var keyTime = parseInt(key);
			if(keyTime < borderTime){
				delete self.laterRead[key];
			}
		}
	}, 10000);
};

(function(){
	//console.log("test")
	
	CommentStreamer.prototype.onTreatComment = function(callback){
		var self = this;
		self.onTreatCommentCallback = callback;
	}
	
	CommentStreamer.prototype.getReadSet = function(timestamp){
		var self = this;
		var ret = self.laterRead[timestamp];
		if(ret == null || ret == undefined){
			ret = new Set();
			self.laterRead[timestamp] = ret;
		}
		
		return ret;
	};
	
	CommentStreamer.prototype.readXml = function(commentXml){
		var self = this;
		for(var i = 0; i < commentXml.length; i++){
			var borderTime = self.startTime + self.border;
			var nodeTime = parseInt(commentXml[i].getAttributeNode("time").value);
			var nodeNum = commentXml[i].getAttributeNode("no").value;
			var readSet;
			var commentStr = (commentXml[i].firstChild) == null ? '':commentXml[i].firstChild.nodeValue;
			
			//console.log("["+nodeTime+"] vs border["+borderTime+"]")
			nodeNum = (nodeNum == null || nodeNum == undefined) ? "":nodeNum;
			//1: borderTimeをnodeTimeが下回る→普通に読み込み対象外。
			if(nodeTime < borderTime){
				continue;
			}
			
			//2: noで既に読み込み済みかどうかを検査。もしそうなら対象外。
			readSet = self.getReadSet(nodeTime);
			//console.log(readSet);
			if(readSet.has(commentStr)){
				continue;
			}
			
			if(self.startTime < nodeTime){
				self.startTime = nodeTime;
			}
			readSet.add(commentStr);
			
			if(!self.isFirst){
				self.onTreatCommentCallback(nodeNum, commentStr, commentXml[i]);
			}
			//break;
		}
		self.isFirst = false;
	};
	
})();

/**
 *	\brief		「いっぺんにコメントを入れた場合」への対処。
 *				キューを作り、一定時間ごとに表示させる。
 */
CommentGeneratorQueue = function(period){
	var self = this;
	this.period = parseInt(period);
	this.onDequeueFunc = function(CGen) {};
	this.queue = [];
	this.limit = -1;
	if(self.period <= 0){
		self.period = 200;
	}
};

(function(){
	
	var dequeue_ = function(self){
		var cg = self.queue.shift();
		if(cg != null && cg != undefined){
			console.log("dequeued");
			self.onDequeueFunc(cg);
		}
		if(self.queue.length > 0){
			setTimeout(function(){
				dequeue_(self);
			}, self.period);
		}
	}
	
	CommentGeneratorQueue.prototype.onDequeue = function(proc){
		var self = this;
		self.onDequeueFunc = proc;
	};
	
	CommentGeneratorQueue.prototype.pushQueue = function(CGen){
		var self = this;
		if(self.limit >= 0 && self.queue.length <= self.limit){
			return;
		}
		self.queue.push(CGen);
		if(self.queue.length <= 1){
			setTimeout(function(){
				console.log("start to proc");
				dequeue_(self);
			}, self.period);
		}
	}
})();