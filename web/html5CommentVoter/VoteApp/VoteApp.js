VoteApp = function() {
	
	this.vote = {
		method: "POST",
		url: "http://localhost:8084/letters/",
		queue: new AjaxQueue()
	};
	this.search = {
		method: "GET",
		url: "http://localhost:8084/charas/nearest/",
		queue: new AjaxQueue()
	};
	this.onVoted = function(subject, subjNo, orgNum, num) {};
};

(function(){
	
	var vote_ = function(self, id, num, conn, handle, orgNum, orgSubj){
		var params = {
			voter_name: handle,
			voter_address: conn,
			voted_name: orgSubj,
			chara_variation: "",
			voted_num: num,
			remarks: "自動投票[" + orgNum + "]",
			chara_id: id
		}
		//console.log(params);
		self.vote.queue.pushRequest(
			self.vote.method, self.vote.url, params,
			function(data){
				//console.log("VOTED!!")
				self.onVoted(params.voted_name, id, orgNum, num);
			}
		);
	};
	
	VoteApp.prototype.voteFromStr = function(str, conn, handle) {
		var self = this
		//var re = /(vote|投票)\s*:\s*([^,/|\s]+)\s*[>:,/|](.+)/
		var re = /(vote|投票|ｖｏｔｅ)\s*[:：]\s*([^>:：,，、/／|｜\s]+)\s*[>:：,，、/／|｜]?\s*(.+)/i
		var matched = str.match(re)
		if(matched == null || matched == undefined){
			console.log("unmatched...");
			return false;
		}
		
		var subject = matched[2];
		var orgNum = matched[3]
		var num = numberParser(orgNum);
		if(num == "NaN"){
			console.log(orgNum + "->Not a number");
			return false;
		}
		
		//名前で書かれた場合、「最も近いもの」を見つける処理をキューする。
		if(isNaN(subject)){
			self.search.queue.pushRequest(
				self.search.method, self.search.url, {name: subject},
				function(data){
					//見つけ次第、投票。
					console.log("searched! [" + subject + "->" + data + "]")
					vote_(self, data, num, conn, handle, orgNum, subject);
				}
			);
		}
		else{
			console.log("direct vote")
			vote_(self, subject, num, conn, handle, orgNum, subject)
		}
		return true;
	};
	

})();